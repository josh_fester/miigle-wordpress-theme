<?php
/**
 * Template Name: About
 */
?>

<div id="template-about">
  <?php $bannerimg = get_field('ta_banner_image'); ?>
  <section id="splash" class="text-center page-splash" style="background-image:url(<?php echo $bannerimg['url']; ?>)">
    <div class="container">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <?= the_field('ta_hero_text'); ?>
        </div>
      </div>
    </div>
  </section>
  
  <section id="about">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <?php while (have_posts()) : the_post(); ?>
            <?php 
				$getPost = get_the_content();
				$postwithbreaks = wpautop( $getPost, true );
				$withAdditional = apply_filters('the_content', $postwithbreaks);
				echo $withAdditional;
			?>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
  </section>
  
  <section id="team" class="text-center">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <?= the_field('ta_team_header'); ?>
          
			<div class="row flex--justify-center">
			<?php if( have_rows('ta_team_member_list') ): $i = 0;
			  $last = count(get_field('ta_team_member_list'));
			  while( have_rows('ta_team_member_list') ): the_row(); $i++;
			  // vars
			  $headshot = get_sub_field('ta_member_headshot');
			  $info = get_sub_field('ta_member_info');
			?>
				<div class="col-sm-4 team">
					<p><img class="img-responsive" src="<?php echo $headshot['url']; ?>"></p>
					<?php echo $info; ?>
				</div>
				<?php if (($i % 3)==0 && $i!=$last): ?>
			</div>
			<div class="row flex--justify-center">
			<?php endif;
				  endwhile;
			  endif;
			?>
			</div>
			
        </div>
      </div>
    </div>
  </section>
  
  <section id="video" class="text-center page-video">
    <div class="container">
      <?= the_field('ta_video_text'); ?>
    </div>
  </section>
  
  <div class="modal fade page-video" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-body">
          <iframe src="https://player.vimeo.com/video/77944311" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
  
</div>
