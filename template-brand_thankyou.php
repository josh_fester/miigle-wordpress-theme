<?php
/**
 * Template Name: Brand Thank You
 */
 
$user = wp_get_current_user();
?>

<div id="template-brand_thankyou">
  
 	<section id="brand_thankyou" class="mT mB"> 
   	<div class="container"> 
			<div class="row flexRow mB">        
				<div class="col-sm-1 text-sm-center">
					<img src="<?= get_template_directory_uri() ?>/assets/images/pl-icon-success.png">
				</div>
				<div class="col-sm-8 text-sm-center">
        	<?php the_field('bt_page_heading'); ?>
        </div>
        <div class="col-sm-3 text-sm-center">
        	<a class="btn btn-default btn-cta" href="<?php the_field('bt_thank_you_cta_link'); ?>"><?php the_field('bt_thank_you_cta_link_text'); ?></a>
        </div>
			</div>
			
				<hr>
			
			<div class="row mB mT">        
				<div class="col-sm-12 whats-next text-sm-center">
        	<?php the_field('bt_whats_next_content'); ?>
        </div>
			</div>	
			
				<hr>
		</div>
  </section>
  
  <?php require_once(locate_template('templates/brand/slider-category-desktop.php')); ?>
  <?php require_once(locate_template('templates/brand/slider-category-mobile.php')); ?>
     
</div>

<!-- Google Code for Directory Listing Purchase Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 865326045;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "520OCPuZ0GwQ3afPnAM";
var google_conversion_value = 49.99;
var google_conversion_currency = "USD";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/865326045/?value=49.99&amp;currency_code=USD&amp;label=520OCPuZ0GwQ3afPnAM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
