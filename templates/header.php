<nav class="primary-nav navbar navbar-default navbar-fixed-top">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <?php 
      // Get menu object
      $theme_location = 'primary_navigation_collapsed';
      $theme_locations = get_nav_menu_locations();
      $collapsed_menu = get_term( $theme_locations[$theme_location], 'nav_menu' );
      // Echo count of items in menu
      if ( $collapsed_menu->count > 0 ) { ?>
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mgl-primary-collapsed" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      <?php
        }
        wp_nav_menu( array(
            'menu'           => 'primary_navigation_featured',
            'theme_location' => 'primary_navigation_featured',
            'depth'          => 1,
            'container'      => 'div',
            'container_class'   => 'featured-nav',
            //'container_id'      => 'bs-example-navbar-collapse-1',
            'menu_class'     => 'hidden-xs hidden-sm',
            'fallback_cb'    => 'wp_bootstrap_navwalker::fallback',
            'walker'         => new wp_bootstrap_navwalker()
          )
        );
      ?>
      <a class="navbar-brand" href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo.svg" alt="Miigle Logo"></a>
    </div>

    <?php
      wp_nav_menu( array(
          'menu'           => 'primary_navigation_collapsed',
          'theme_location' => 'primary_navigation_collapsed',
          'depth'          => 1,
          'container'      => 'div',
          'container_class'   => 'collapsed-nav collapse navbar-collapse',
          'container_id'      => 'mgl-primary-collapsed',
          'menu_class'     => '',
          'fallback_cb'    => 'wp_bootstrap_navwalker::fallback',
          'walker'         => new wp_bootstrap_navwalker()
        )
      );
    ?>
  </div>
</nav>
