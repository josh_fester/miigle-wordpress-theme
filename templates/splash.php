<section id="splash">
  <div class="container text-center">
    <?php the_field('index_hero_text', get_option('page_for_posts')); ?>
  </div>
</section>
