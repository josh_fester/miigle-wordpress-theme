          <div class="row row--pad" id="filter-product">
              <div class="col-nano-12 col-md-5">
                  <p class="text-upper fitler-txt">filter by</p>
                  <form class="form-inline row" id="product-filter">
                      <div class="form-group filter-cat custom-select col-nano-8"> <!-- .custom-select -->
                          <select class="form-control" name="category" id="category-select">
                              <option value="all" data-reactid="">PRODUCT CATEGORIES</option>
                              <option value="all" data-reactid="">All</option>
                              <option value="accessories" data-reactid="">Accessories</option>
                              <option value="beauty" data-reactid="">Beauty</option>
                              <option value="clothing" data-reactid="">Clothing</option>
                              <option value="electronics" data-reactid="">Electronics</option>
                              <option value="foods" data-reactid="">Foods</option>
                              <option value="gifts" data-reactid="">Gifts</option>
                              <option value="health" data-reactid="">Health</option>
                              <option value="home" data-reactid="">Home</option>
                              <option value="kids" data-reactid="">Kids</option>
                              <option value="jewelry" data-reactid="">Jewelry</option>
                              <option value="pets" data-reactid="">Pets</option>
                              <option value="shoes" data-reactid="">Shoes</option>
                          </select>
                      </div>
                      <div class="form-group filter-type custom-select col-nano-4"> <!-- .custom-select -->
                          <select class="form-control" name="persona" id="persona-select">
                              <option value="all">PERSONA</option>
                              <option value="all" data-reactid="">All</option>
                              <option value="women" data-reactid="">Women</option>
                              <option value="men" data-reactid="">Men</option>
                              <option value="kids" data-reactid="">Kids</option>
                          </select>
                      </div>
                  </form>
              </div>
          </div>