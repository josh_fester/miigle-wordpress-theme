            <div class="row">
                <div class="col-nano-6 col-lg-3 card-grid">
                    <div class="cardThumb flex-column d-flex h-100">
                        <div class="cat-thumb" style="background-image:url('<?= get_template_directory_uri() ?>/assets/images/brand-accessories.png');">
                			<div class="mask accessories"></div>
              			</div>
                        <div class="caption text-upper">
                            <h3>Accessories</h3>
                        </div>
                        <a href="/marketplace?category=accessories">
                            <div class="mask"></div>
                        </a>
                    </div>
                </div>
                <div class="col-nano-6 col-lg-3 card-grid">
                    <div class="cardThumb flex-column d-flex h-100">
                        <div class="cat-thumb" style="background-image:url('<?= get_template_directory_uri() ?>/assets/images/brand-beauty.png');">
                			<div class="mask beauty"></div>
              			</div>
                        <div class="caption text-upper">
                            <h3>Beauty</h3>
                        </div>
                        <a href="/marketplace?category=beauty">
                            <div class="mask"></div>
                        </a>
                    </div>
                </div>
                <div class="col-nano-6 col-lg-3 card-grid">
                    <div class="cardThumb flex-column d-flex h-100">
                        <div class="cat-thumb" style="background-image:url('<?= get_template_directory_uri() ?>/assets/images/brand-clothing.png');">
                			<div class="mask clothing"></div>
              			</div>
                        <div class="caption text-upper">
                            <h3>Clothing</h3>
                        </div>
                        <a href="/marketplace?category=clothing">
                            <div class="mask"></div>
                        </a>
                    </div>
                </div>
                <div class="col-nano-6 col-lg-3 card-grid">
                    <div class="cardThumb flex-column d-flex h-100">
                        <div class="cat-thumb" style="background-image:url('<?= get_template_directory_uri() ?>/assets/images/brand-electronics.png');">
                			<div class="mask electronics"></div>
              			</div>
                        <div class="caption text-upper">
                            <h3>Electronics</h3>
                        </div>
                        <a href="/marketplace?category=electronics">
                            <div class="mask"></div>
                        </a>
                    </div>
                </div>
                <div class="col-nano-6 col-lg-3 card-grid">
                  <div class="cardThumb flex-column d-flex h-100">
                      <div class="cat-thumb" style="background-image:url('<?= get_template_directory_uri() ?>/assets/images/brand-foods.png');">
                          <div class="mask foods"></div>
                      </div>
                      <div class="caption text-upper">
                          <h3>Foods</h3>
                      </div>
                      <a href="/marketplace?category=foods">
                          <div class="mask"></div>
                      </a>
                  </div>
                </div>
                <div class="col-nano-6 col-lg-3 card-grid">
                    <div class="cardThumb flex-column d-flex h-100">
                        <div class="cat-thumb" style="background-image:url('<?= get_template_directory_uri() ?>/assets/images/brand-gifts.png');">
                			<div class="mask gifts"></div>
              			</div>
                        <div class="caption text-upper">
                            <h3>Gifts</h3>
                        </div>
                        <a href="/marketplace?category=gifts">
                            <div class="mask"></div>
                        </a>
                    </div>
                </div>
                <div class="col-nano-6 col-lg-3 card-grid">
                    <div class="cardThumb flex-column d-flex h-100">
                        <div class="cat-thumb" style="background-image:url('<?= get_template_directory_uri() ?>/assets/images/brand-health.png');">
                			<div class="mask health"></div>
              			</div>
                        <div class="caption text-upper">
                            <h3>Health</h3>
                        </div>
                        <a href="/marketplace?category=health">
                            <div class="mask"></div>
                        </a>
                    </div>
                </div>
                <div class="col-nano-6 col-lg-3 card-grid">
                    <div class="cardThumb flex-column d-flex h-100">
                        <div class="cat-thumb" style="background-image:url('<?= get_template_directory_uri() ?>/assets/images/brand-home.png');">
                			<div class="mask home"></div>
              			</div>
                        <div class="caption text-upper">
                            <h3>Home</h3>
                        </div>
                        <a href="/marketplace?category=home">
                            <div class="mask"></div>
                        </a>
                    </div>
                </div>
                <div class="col-nano-6 col-lg-3 card-grid">
                    <div class="cardThumb flex-column d-flex h-100">
                        <div class="cat-thumb" style="background-image:url('<?= get_template_directory_uri() ?>/assets/images/brand-jewelry.png');">
                			<div class="mask jewelry"></div>
              			</div>
                        <div class="caption text-upper">
                            <h3>Jewelry</h3>
                        </div>
                        <a href="/marketplace?category=jewelry">
                            <div class="mask"></div>
                        </a>
                    </div>
                </div>
                <div class="col-nano-6 col-lg-3 card-grid">
                    <div class="cardThumb flex-column d-flex h-100">
                        <div class="cat-thumb" style="background-image:url('<?= get_template_directory_uri() ?>/assets/images/brand-kids.png');">
                			<div class="mask kids"></div>
              			</div>
                        <div class="caption text-upper">
                            <h3>Kids</h3>
                        </div>
                        <a href="/marketplace?category=kids">
                            <div class="mask"></div>
                        </a>
                    </div>
                </div>
                <div class="col-nano-6 col-lg-3 card-grid">
                    <div class="cardThumb flex-column d-flex h-100">
                        <div class="cat-thumb" style="background-image:url('<?= get_template_directory_uri() ?>/assets/images/brand-pets.png');">
                			<div class="mask pets"></div>
              			</div>
                        <div class="caption text-upper">
                            <h3>Pets</h3>
                        </div>
                        <a href="/marketplace?category=pets">
                            <div class="mask"></div>
                        </a>
                    </div>
                </div>
                <div class="col-nano-6 col-lg-3 card-grid">
                    <div class="cardThumb flex-column d-flex h-100">
                        <div class="cat-thumb" style="background-image:url('<?= get_template_directory_uri() ?>/assets/images/brand-shoes.png');">
                			<div class="mask shoes"></div>
              			</div>
                        <div class="caption text-upper">
                            <h3>Shoes</h3>
                        </div>
                        <a href="/marketplace?category=shoes">
                            <div class="mask"></div>
                        </a>
                    </div>
                </div>  
            </div>