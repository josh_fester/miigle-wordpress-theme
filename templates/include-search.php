<div class="container">
    <div class="search-wrapper">
        <form id="product-search" method="GET" action="/search">
            <div class="row">
                <div class="col-nano-9">
                    <div class="input-group">
                        <span class="input-group-addon"><img src="/wp-content/uploads/2018/07/icon-search.svg"></span>
                        <input type="search" class="form-control" name="search" placeholder="Search ethical products and brands…">
                    </div>
                </div>
                <div class="col-nano-3">
                    <button type="submit" class="btn btn-default btn-submit">SEARCH</button>
                </div>
            </div>
        </form>
    </div>
</div>