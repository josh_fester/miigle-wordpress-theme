<div id="template-404">
  
  <section id="page404" class="text-center">
    <div class="container">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
        	<h3><?php _e('Ooops!', 'sage'); ?></h3>	
        	<p><?php _e('We can’t seem to find the page you are looking for.', 'sage'); ?></p> 
        	<p><?php _e('Here are some helpful links:', 'sage'); ?>  <a href="<?= home_url() ?>">Home</a>  |  <a href="<?= home_url() ?>/about">About</a>  |  <a href="<?= home_url() ?>/directory">Directory</a></p> 
        </div>
      </div>
    </div>
  </section>
  
</div>
