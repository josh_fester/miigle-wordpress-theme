<?php
/**
 * Template Name: Brand Submit
 */

$user                   = wp_get_current_user();
$braintree_client_token = Braintree_ClientToken::generate();

?>

<div id="braintree-client-token" data-token="<?= $braintree_client_token ?>"></div>

<div id="template-brand_submit">

    <section id="brand_price" class="pT pB">
        <div class="container">
            <div class="row borderB">
                <!-- brand detail title section-->
                <div class="col-sm-4 text-sm-center white-text">
					<?php the_field( 'bs_page_header' ); ?>
                </div>
                <div class="col-sm-1">
                    &nbsp;
                </div>
                <div class="col-sm-7 white-text priceList">
					<?php the_field( 'bs_benefit_content' ); ?>
                </div>
            </div>
            <div class="row borderB companyLogo">
                <div class="col-sm-10 col-sm-offset-1 text-center white-text">
					<?php the_field( 'bs_supported_company_content' ); ?>
                </div>
            </div>
            <!-- price -->
            <div class="row msB">
                <div class="col-sm-3 text-center">
                    <div class="well <?php the_field( 'bs_pricing_card_price_1_recommended' ); ?>">
                        <span class="badge"><?php the_field( 'bs_pricing_card_price_1_recommended' ); ?></span>
						<?php the_field( 'bs_pricing_card_price_1' ); ?>
                    </div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="well <?php the_field( 'bs_pricing_card_price_2_recommended' ); ?>">
                        <span class="badge"><?php the_field( 'bs_pricing_card_price_2_recommended' ); ?></span>
						<?php the_field( 'bs_pricing_card_price_2' ); ?>
                    </div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="well <?php the_field( 'bs_pricing_card_price_3_recommended' ); ?>">
                        <span class="badge"><?php the_field( 'bs_pricing_card_price_3_recommended' ); ?></span>
						<?php the_field( 'bs_pricing_card_price_3' ); ?>
                    </div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="well <?php the_field( 'bs_pricing_card_price_4_recommended' ); ?>">
                        <span class="badge">Recommended</span>
						<?php the_field( 'bs_pricing_card_price_4' ); ?>
                    </div>
                </div>
            </div>
			<?php if ( ! the_field( 'bs_promotional_content' ) ): ?>
                <!-- info -->
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 text-center white-text">
						<?php the_field( 'bs_promotional_content' ); ?>
                    </div>
                </div>
			<?php endif; ?>
        </div>
    </section>

    <form id="checkout-form" method="post" action="mgl/v1/subscription/new">
        <input type="hidden" name="payment-method-nonce" id="nonce">
        <section class="checkout-section">
            <div class="container">
                <header class="checkout-section__header">
                    <h2><strong>Step 1:</strong> Select pricing plan</h2>
                </header>

                <div class="row checkout-pricing-items"></div>
            </div>
        </section>

        <section class="checkout-section">
            <div class="container">
                <header class="checkout-section__header">
                    <h2><strong>Step 2:</strong> Tell us about your brand</h2>
                </header>

                <div class="row checkout-user">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="first-name">First Name</label>
                            <input type="text" class="form-control" id="first-name" name="firstName" placeholder="John">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="last-name">Last Name</label>
                            <input type="text" class="form-control" id="last-name" name="lastName" placeholder="Doe">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email-address">Email Address</label>
                            <input type="text" class="form-control" id="email-address" name="email"
                                   placeholder="john.doe@me.com">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="phone-number">Phone Number</label>
                            <input type="text" class="form-control" id="phone-number" name="phone"
                                   placeholder="999-999-9999">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="brand-name">Brand Name</label>
                            <input type="text" class="form-control" id="brand-name" name="brandName" placeholder="Toms">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="job-title">Job Title</label>
                            <input type="text" class="form-control" id="job-title" name="job"
                                   placeholder="VP, Marketing">
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="website-url">Website URL</label>
                            <input type="text" class="form-control" id="website-url" name="website"
                                   placeholder="www.toms.com">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="social-textarea">Social or Ecological Impact (140 characters max)</label>
                            <textarea name="impact" class="form-control" id="social-textarea"
                                      placeholder="For every item that you purchase, we donate one to a person in need."></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="checkout-section">
            <div class="container">
                <header class="checkout-section__header">
                    <h2><strong>Step 3:</strong> Categories (select all that apply)</h2>
                </header>

                <div class="checkout-categories">
                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-14" value="14">
                        <label class="category" for="category-14">Accessories</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-11" value="11">
                        <label class="category" for="category-11">Beauty</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-8" value="8">
                        <label class="category" for="category-8">Clothing</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-60" value="60">
                        <label class="category" for="category-60">Food</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-17" value="17">
                        <label class="category" for="category-17">Gifts</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-41" value="41">
                        <label class="category" for="category-41">Health</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-9" value="9">
                        <label class="category" for="category-9">Home</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-18" value="18">
                        <label class="category" for="category-18">Jewelry</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-10" value="10">
                        <label class="category" for="category-10">Kids</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-27" value="27">
                        <label class="category" for="category-27">Pets</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-24" value="24">
                        <label class="category" for="category-24">Shoes</label>
                    </div>
                </div>
            </div>
        </section>

        <section class="checkout-section checkout-section--payment">
            <div class="container">
                <header class="checkout-section__header checkout-section__header--w-subtitle">
                    <h2><strong>Step 4:</strong> Enter your payment information</h2>
                    <p>Every submission is reviewed by a member of our team. Your card will not be charged until listing is approved.</p>
                </header>

                <div class="row checkout-cards">
                    <div class="col-md-2 checkout-cards__item">
                        <input type="radio" name="payment" value="visa" id="visa" checked>
                        <label for="visa"><img src="<?= get_template_directory_uri(); ?>/dist/images/visa-card.svg"
                                               alt="Visa"></label>
                    </div>

                    <div class="col-md-2 checkout-cards__item">
                        <input type="radio" name="payment" value="mastercard" id="mastercard">
                        <label for="mastercard"><img
                                    src="<?= get_template_directory_uri(); ?>/dist/images/mastercard-card.svg"
                                    alt="Mastercard"></label>
                    </div>

                    <div class="col-md-2 checkout-cards__item">
                        <input type="radio" name="payment" value="amex" id="amex">
                        <label for="amex"><img src="<?= get_template_directory_uri(); ?>/dist/images/amex-card.svg"
                                               alt="Amex"></label>
                    </div>

                    <div class="col-md-2 checkout-cards__item">
                        <input type="radio" name="payment" value="paypal" id="paypal">
                        <label for="paypal"><img src="<?= get_template_directory_uri(); ?>/dist/images/paypal-card.svg"
                                                 alt="PayPal"></label>
                    </div>
                </div>

                <div class="row checkout-cards-fields">
                    <div class="col-md-4">
                        <div class="form-group required">
                            <label for="card-number">Credit Card Number</label>
                            <div id="card-number" class="hosted-field form-control"></div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group required">
                            <label for="cvv">CVV Code (3 or 4 digit)</label>
                            <div id="cvv" class="hosted-field form-control"></div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group required">
                            <label for="postal-code">Postal/Zip</label>
                            <div id="postal-code" class="hosted-field form-control"></div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group required">
                            <label for="expiration-date">Expiration</label>
                            <div id="expiration-date" class="hosted-field form-control"></div>
                        </div>
                    </div>

                    <div class="col-md-8 checkout-cards-fields__code">
                        <div class="form-group flex">
                            <div class="flex__fill-gap">
                                <label for="promo-code">Promo Code</label>
                                <input type="text" class="form-control" id="promo-code" name="promo">
                            </div>

                            <button type="button" class="btn">Apply</button>
                        </div>
                    </div>
                </div>

                <div class="row checkout-paypal-fields" style="display: none;">
                    <div class="col-md-12 checkout-cards-fields__code">
                        <div class="form-group flex">
                            <div class="flex__fill-gap">
                                <label for="promo-code">Promo Code</label>
                                <input type="text" class="form-control" id="paypal-promo-code" name="promo" disabled>
                            </div>

                            <button type="button" class="btn">Apply</button>
                        </div>
                    </div>
                </div>

                <div class="checkout-total">
                    <p><strong>Total:</strong> <span class="checkout-plan__name"></span> - $<span class="checkout-total__price"></span></p>

                    <button type="submit">Submit Your Brand <span class="fa fa-chevron-right"></span></button>

                    <div class="checkout-total__loader hidden">
                        <img src="<?= get_template_directory_uri(); ?>/dist/images/ajax-loader.gif">
                    </div>
                </div>
            </div>
        </section>
    </form>

</div>
