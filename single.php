<?php
/**
 * Template Name: Brand Page - Marketplace
 */

use Miigle\Models\Brand;
use Miigle\Models\Product;

$badges = Brand\get_badges(get_the_ID());
//$user = wp_get_current_user();
?>

<div id="template-brand_page" class="v2">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <!-- Brand banner -->
    <?php $hero = Brand\get_main_img(get_the_ID());
      
          if ($hero) {
            $hero = $hero;
          }else{
            $hero = "/wp-content/themes/sage/dist/images/hero-cards-bg.png";
          }
    ?>
    <section id="brand_page_hero" style="background-image:url('<?php echo $hero ?>');">
        <div class="container h100">
            <div class="row flex--justify-center flex--align-bottom h100">
                <div class="col-nano-8 text-center text-upper bgWhite85 pb1 pt1 sr-only">
                    <!-- Social Impact Text -->
                    <h3>Great job, you’re visiting a Social Good brand!</h3>
                </div>
            </div>
        </div>
    </section>

    <section id="brand_page_title">
        <div class="container">
            <div class="row flex--justify-center">
                <div class="col-nano-10 col-md-6 text-center text-upper pt3">
                    <!-- Brand name -->
                    <h1>
                        <?php the_title() ?>
                    </h1>
                </div>
            </div>
            <div class="row flex--justify-center">
                <div class="col-nano-12 col-md-8 lead mb0 text-center pb3">
                    <!-- Brand Description Text -->
                    <p>
                        <?php echo Brand\brand_impact(get_the_ID()) ?>
                    </p>
                </div>
            </div>
        </div>
    </section>

  <section id="brand_page">
        <div class="container">
            <div class="row">
              <?php if(Brand\has_info(get_the_ID())) { ?>
              
              <?php } else { ?>
              <div class="col-md-5 brand-media">
                  <div class="brand--thumb pb1">
                      <!-- Featured Image -->
                      <img class="alignnone size-full wp-image-22558" src="<?php echo get_the_post_thumbnail_url() ?>"
                           alt="" srcset="<?php echo get_the_post_thumbnail_url() ?>"
                           sizes="(max-width: 703px) 100vw, 703px">
                      <!-- If Video embed URL exsit replace with Video -->
                  </div>
                  <div class="brand--tags">
                      <!-- Badges -->

                      <?php foreach (Brand\get_badges(get_the_ID()) as $badge): ?>

                          <a href="<?= home_url() ?>/badge/<?php echo $badge->slug ?>" class="btn btn-tags text-upper"
                             title="<?php echo $badge->slug ?>">
                              <?php echo $badge->name ?>
                          </a>

                      <?php endforeach; ?>

                  </div>
              </div>
              <div class="col-md-7 brand-data">
                <div class="prod--description">
                  <div class="prod--entry">
                     <?php
						$getPost = get_the_content();
						$postwithbreaks = wpautop( $getPost, true );
						$withAdditional = apply_filters('the_content', $postwithbreaks);
						echo $withAdditional;
					 ?>
                  </div>
                </div>
              </div>
              <?php } ?>
            </div>
        </div>
    </section>

    <?php if ( !empty( Brand\get_quote(get_the_ID()) ) ) { ?>
    <section id="brand_page_quote" class="mt5">
        <div class="container">
            <div class="row">
                <div class="col-nano-12 text-center quoteText border pt3 pb3">
                    <!-- Brand Quote Text -->
                    <p>
                        <?php echo Brand\get_quote(get_the_ID()) ?>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <?php } ?>
  
    <section id="section--search-bar" class="pt5">
        <?php get_template_part('templates/include', 'search'); ?>
    </section>

    <section id="section--similar-products" class="section--pad">
      <?php if(Brand\has_product(get_the_ID())) { ?>
        <div class="container">
          <div class="row">
            <div class="col-nano-12 text-center brand-coming-soon pt5 pb3">
              <?php echo Brand\get_coming_soon(get_the_ID()) ?>
            </div>
          </div>
        </div>
      <?php } else { ?>
      <div class="container">
            <!-- FILTER ROW -->
            <div class="row" id="filter-product">
                <div class="col-nano-12 text-center text-upper pt1 pb2">
                    <span class="cat-filter active"><a class="category" title="all" href="#all">all</a></span>

                    <?php foreach (Brand\get_brand_categories(get_the_ID()) as $category): ?>
                    <?php if (!($category->parent)): ?>
                            <span class="cat-filter">
                        <a href="<? echo $category->name ?>" class="category"
                           title="<?php echo $category->slug ?>">
                            <?php echo $category->name ?>
                        </a>
                    </span>

                            <?php endif; ?>
                    <?php endforeach; ?>

                </div>
            </div>

            <div class="row masonry-container" id="category-container">
                <div class="col-nano-12 col-micro-6 col-xs-6 col-sm-6 col-md-4 col-lg-3 masonry-sizer"></div>
                <!-- left empty for column sizing -->

                <?php
                wp_reset_postdata();

                $args = array('post_type' => 'mgl_product', 'meta_key' => '_mgl_product_brand_id', 'meta_value' => get_the_ID(), 'posts_per_page' => -1, 'post_status' => 'publish');
                $the_query = new WP_Query($args);

                if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
                    ?>

                    <div class="col-lg-3 col-md-4 col-nano-6 masonry-item">
                        <div class="thumbnail">
                            <img src="<?php echo Product\get_thumbnail(get_the_ID()) ?>" class="">
                            <div class="caption">
                                <p class="brand">
                                    <?php echo Product\get_brand_title(get_the_ID()) ?>

                                </p>

                                <h3><?php the_title() ?></h3>
                                <p class="price"><?= Product\get_price(get_the_ID()) ?></p>
                            </div>
                            <a href="<?php echo get_the_permalink() ?>">
                                <div class="mask"></div>
                            </a>
                        </div>
                    </div>
                <?php endwhile; ?>
                <?php endif;
                wp_reset_postdata(); ?>


            </div>

            <div class="row view-more" id="viewProducts">
                <div class="col-nano-12 text-center">
                    <a class="btn-view-more text-upper" href="<?php echo site_url('marketplace') ?>">View More</a>
                </div>
            </div>

        </div>
      <?php } ?>
    </section>
<?php endwhile; endif; ?>
</div>

<script type="text/javascript">

    jQuery(document).ready(function() {


        var cat = '';

        var brandID = <?php echo get_the_ID() ?> ;

        // category buttons
        jQuery(".category").on('click', function (e) {
            e.preventDefault();
            cat = jQuery(this).attr('title');
            if (cat === 'all')
                cat = '';
            jQuery('.category').parent().siblings().removeClass('active');
            jQuery(this).parent().addClass('active');
            pageCounter = 0;
            getPostsByCategory();
        });

        getPostsByCategory();

        var first = true;
        masonry2();
        first = false;

        function getPostsByCategory(){

            /**
             * Ajax call
             */

            jQuery.when(
                jQuery.ajax({
                    url: '/getBrandPosts.php?category="' + cat + '"' + '&brandID="' + brandID + '"',
                    success: function(response){
                        var data = jQuery.parseJSON(response);
                        // container selector
                        var container = jQuery("#category-container");
                        container.html(' <div class="col-nano-12 col-micro-6 col-xs-6 col-sm-6 col-md-4 col-lg-3 masonry-sizer"></div>');

                        // Displays data with an .each loop
                        jQuery.each(data, function (key, product)
                        {
                            container.append('<div class="col-lg-3 col-md-4 col-nano-6 masonry-item">'+
                                '<div class="thumbnail">'+
                                '<img src="' + product.thumbnail + '" class="">'+
                                '<div class="caption">'+
                                '<p class="brand">'+
                                product.brand +
                                '</p>'+

                                '<h3>' +
                                product.title +
                                '</h3>' +
                                '<p class="price">' +
                                product.price +
                                '</p>'+
                                '</div>' +
                                '<a href="' + product.url + '"><div class="mask"></div></a>' +
                                '</div>' +
                                '</div>');
                        });


                    }
                })
            ).then(masonry2);


        }

        function masonry2(){
            var masonry = jQuery('#category-container');

            jQuery(masonry).imagesLoaded( function() {
                jQuery(masonry).masonry({
                    columnWidth: '.masonry-sizer',
                    itemSelector: '.masonry-item',
                    percentPosition: true
                });
            });
           if (!first)
           {
               jQuery(masonry).masonry('reload');
               jQuery(masonry).masonry('layout');
           }
        }
    });

//Brand description	Read More
jQuery(document).ready(function() {
  var closeHeight = '400';
  var moreText 	= 'Read More';
  var lessText	= 'Read Less';
  var duration	= '1000';
  var easing = 'linear';

	jQuery('.prod--description').each(function() {
		
		// Set data attribute to record original height
		var current = jQuery(this).children('.prod--entry');
		current.data('fullHeight', current.height()).css('height', closeHeight + 'px');
        
        var originalHeight = current.data('fullHeight');
		//alert(originalHeight);
      
      if( originalHeight > closeHeight ){
        // Insert "Read More" link
		current.after('<a href="javascript:void(0);" class="btn-read-more text-upper more-link closed">' + moreText + '</a>');
      }

	});
  
  // Link functinoality
	var openSlider = function() {
		link = jQuery(this);
		var openHeight = link.prev('.prod--entry').data('fullHeight') + 'px';
		link.prev('.prod--entry').animate({'height': openHeight}, {duration: duration }, easing);
		link.text(lessText).addClass('open').removeClass('closed');
    	link.unbind('click', openSlider);
		link.bind('click', closeSlider);
	}

	var closeSlider = function() {
		link = jQuery(this);
    	link.prev('.prod--entry').animate({'height': closeHeight}, {duration: duration }, easing);
		link.text(moreText).addClass('closed').removeClass('open');
		link.unbind('click');
		link.bind('click', openSlider);
	}
  
  	// Attach link click functionality
	jQuery('.more-link').bind('click', openSlider);

});
</script>
