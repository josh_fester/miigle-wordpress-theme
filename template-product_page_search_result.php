<?php
/**
 * Template Name: Search Result - Marketplace
 */

use Miigle\Models\Product;
//use Miigle\Models\User;
use Miigle\Models\Brand;
if (isset($_GET['search']))
    $search = $_GET['search'];
else
	$search = '';
//$user = wp_get_current_user();
$postCount = 0;
$postOnly = 0;
?>

<div id="template-search_results" class="v2">
    <section id="section--search-bar" class="pt5">
        <?php get_template_part('templates/include', 'search'); ?>
    </section>
    
    <section id="section--results" class="section--pad">
        <div class="container">
            <!-- TITLE ROW -->
            <div class="row">
                <div class="col-nano-12">
                    <div class="text-sm-left text-center text-upper title--pad">
                        <h1 id="result-amount"></h1>
                    </div>
                </div>
            </div>

            <?php get_template_part('templates/include', 'filter'); ?>
          
            <?php wp_reset_postdata();

            $the_query = new WP_Query(['post_type' => 'mgl_brand', 'meta_key' => '_mgl_brand_is_trending', 's' => $search,  'posts_per_page' => -1]);
            if ($the_query->have_posts()):
            ?>
            <!-- TITLE ROW -->
            <div class="row">
                <div class="col-nano-12">
                    <div class="text-upper title--pad">
                        <h4>Brands</h4>
                    </div>
                </div>
            </div>

            <!-- CONTENT ROW -->
            <div class="row card_rowSpaceL">

                <?php
                if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
                    ?>

                    <?php $postCount++ ?>
                    <div class="col-nano-12 col-sm-6 col-lg-4 card-grid">
                        <a style="text-decoration:none !important" href="<?php echo get_the_permalink() ?>">
                            <div class="card h-100 ">
                                <h3 style="display:inline-block"><?php the_title() ?>
                                    <span class="category">
                              	<?php foreach (Brand\get_brand_categories(get_the_ID()) as $brand): ?>
                                    <?php if (!$brand->parent): ?>
                                        <a href="<?= home_url() ?>/marketplace?category=<?= $brand->name ?>">
											<span style="border: 1px solid #83cbb4; border-radius: 4px; margin-left: 5px; background: none; vertical-align: top; padding: 3px;"
                                                  class="badge catIcon">													<img
                                                        src="<?= get_template_directory_uri() ?>/dist/images/cat_icon-<?= $brand->name ?>.svg"
                                                        alt="<?= $brand->name ?>" title="<?= $brand->name ?>">													</span>
										</a>
                                    <?php endif; ?>
                                <?php endforeach; ?>
								</span>
                                </h3>


                                <p style="text-decoration:none !important; color: #777 !important;">
                                    <?php echo Brand\brand_impact(get_the_ID()) ?>
                                </p>
                            </div>
                        </a>
                    </div>

                <?php endwhile; ?>
                <?php endif;
                wp_reset_postdata(); ?>

            </div>

            <hr>

                <?php else: ?>
                <div class="row">
                    <div class="col-nano-12">
                        <div class="text-upper title--pad">
                            <h4>Brands</h4>
                        </div>
                    </div>
                </div>
                <h5>Sorry, no brands were found matching your search.</h5>

            <?php endif; ?>
            <!-- TITLE ROW -->
            <div class="row pt3">
                <div class="col-nano-12">
                    <div class="text-upper title--pad">
                        <h4>Products</h4>
                    </div>
                </div>
            </div>

            <h5 id="no_results"></h5>
            <!-- CONTENT ROW -->
            <div class="row masonry-container" id="posts-container">
                <div class="col-nano-12 col-micro-6 col-xs-6 col-sm-6 col-md-4 col-lg-3 masonry-sizer"></div>
                <!-- left empty for column sizing -->
                <?php wp_reset_postdata();

                $the_query = new WP_Query(['post_type' => 'mgl_product', 's' => $search,  'posts_per_page' => 40]);

                if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
                    ?>
                    <?php $postCount++ ?>
                    <?php $postOnly++ ?>
                    <div class="col-lg-3 col-md-4 col-nano-6 masonry-item">
                        <div class="thumbnail">
                            <img src="<?php echo Product\get_thumbnail(get_the_ID()) ?>" class="">
                            <div class="caption">
                                <p class="brand">
                                    <?php echo Product\get_brand_title(get_the_ID()) ?>

                                </p>

                                <h3><?php the_title() ?></h3>
                                <p class="price"><?= Product\get_price(get_the_ID()) ?></p>
                            </div>
                            <a href="<?php echo get_the_permalink() ?>">
                                <div class="mask"></div>
                            </a>
                        </div>
                    </div>
                <?php endwhile; ?>
                <?php endif;
                wp_reset_postdata(); ?>


            </div>

            <hr>

            <!-- TITLE ROW -->
            <div class="row pt3">
                <div class="col-nano-12">
                    <div class="text-upper title--pad">
                        <h4>Categories</h4>
                    </div>
                </div>
            </div>

            <!-- CONTENT ROW -->
            <?php get_template_part('templates/include', 'category-list'); ?>
          
        </div>
    </section>

</div>

<script type="text/javascript">

    jQuery(document).ready(function() {

        var first = true;
        var counter = 0;
        var amount = <?php echo $postCount ?>;
        var postAmount = <?php echo $postOnly ?>;
        var search = '<?php echo $_GET['search'] ?>';

        getPosts();
        jQuery('#result-amount').append(amount + ' results found');

        var category = document.getElementById('category-select');
        var subcategory = document.getElementById('persona-select');

       
      jQuery(".select-items").on('click', function () {
            firstLoad = true;
            getPosts();
        });
        function getPosts(){
            if (first)
            {
                amount -= postAmount;
                first = false;
            }
            else{
                amount -= counter;
                counter = 0;
            }

            //var cat = jQuery("#category-select option:selected").val();
            //var persona = jQuery("#persona-select option:selected").val();
            var cat = jQuery(".category-select").attr("value");
            var persona = jQuery(".persona-select").attr("value");
            var scope = '';

            if (persona !== '' && cat !== '' && cat !== 'all')
                scope = persona + '-' + cat;
            else if(persona === '')
                scope = cat;
            else if(persona === '' && cat === '')
                scope = 'all';
            else
                scope = persona;



            jQuery.when(
                jQuery.ajax({
                    url: '/filterposts.php?scope="' + scope + '"&search="' + search + '"',
                    success: function(response){
                        var data = jQuery.parseJSON(response);


                        var container = jQuery("#posts-container");
                        container.html(' <div class="col-nano-12 col-micro-6 col-xs-6 col-sm-6 col-md-4 col-lg-3 masonry-sizer"></div>');
                        container.css({'height' : 'auto'});
                        jQuery.when(  jQuery.each(data, function (key, product)
                        {
                            counter++;
                            container.append('<div class="col-lg-3 col-md-4 col-nano-6 masonry-item">'+
                                '<div class="thumbnail">'+
                                '<img src="' + product.thumbnail + '" class="">'+
                                '<div class="caption">'+
                                '<p class="brand">'+
                                product.brand +
                                '</p>'+

                                '<h3>' +
                                product.post_title +
                                '</h3>' +
                                '<p class="price">' +
                                product.price +
                                '</p>'+
                                '</div>' +
                                '<a href="' + product.guid + '"><div class="mask"></div></a>' +
                                '</div>' +
                                '</div>');
                        })).then(function( ) {
                            amount += counter;
                            if (counter === 0)
                                {
									container.html(' <div class="col-nano-12 col-micro-6 col-xs-6 col-sm-6 col-md-4 col-lg-3 masonry-sizer"></div>');
									jQuery('#no_results').text('Sorry, no products were found matching your search.');
								}

                            jQuery('#result-amount').text('');

                            jQuery('#result-amount').append('<span class="clr">' + amount + '</span> results found');
                        });




                    }
                })
            ).then(masonry2);



        }

        function masonry2(){
            var masonry = jQuery('#posts-container');

            jQuery(masonry).imagesLoaded( function() {
                jQuery(masonry).masonry({
                    columnWidth: '.masonry-sizer',
                    itemSelector: '.masonry-item',
                    percentPosition: true
                });
            });
            jQuery(masonry).masonry('reload');
            jQuery(masonry).masonry('layout');

        }
    });



</script>