<?php
/**
 * Template Name: Brand - Marketplace
 */

use Miigle\Models\Product;
//use Miigle\Models\User;
use Miigle\Models\Brand;
use Miigle\Helpers;
//$mgl_current_user = User\current();
?>
<!-- Notification Bar -->
<?php if ( get_field( 'notification_enable', 'option' ) ) : ?>
    <div class="notice flex--align-vertical " id="nBar">
        <div class="container">
            <div class="row flex--align-vertical">
				<div class="col-sm-8 notification-text">
                    <div class="h5 mb0 mt0"><?php the_field( 'notification_text', 'option' ); ?></div>
                </div>

				<?php if ( $link = get_field( 'notification_link', 'option' ) ) : ?>
					<div class="col-sm-4 notification-link text-center">
                        <a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="btn btn--white"><?php echo $link['title']; ?></a>
                    </div>
				<?php endif; ?>
            </div>
        </div>

        <a class="notice__close icon-close"><img src="/wp-content/uploads/2018/09/times-circle-light.png"></a>
    </div>
<?php endif; ?>

<div id="template-brand_directory" class="v2">
	
    <section id="section--hero">
      <?php if ( get_field( 'mkp_miigle_messages' ) ) : ?>
        <div class="message hidden-xs">
          <div class="container text-center text-white text-upper"><?php the_field('mkp_miigle_messages'); ?></div>
        </div>
      <?php endif; ?> 
        <div id="carousel-hero" class="carousel slide carousel-fade" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators container">
				<?php if( have_rows('mkp_hero_brand') ): $i = -1;
				while( have_rows('mkp_hero_brand') ): the_row(); $i++;

				if ( $i == 0 ){
					$class = 'active';
				} else {
					$class='';
				}
				?>
				<li data-target="#carousel-hero" data-slide-to="<?php echo $i; ?>" class="<?php echo $class; ?>"></li>
				<?php
				endwhile;
				endif;
				?>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<?php if( have_rows('mkp_hero_brand') ): $i = -1;
				while( have_rows('mkp_hero_brand') ): the_row(); $i++;
				// vars
				$bpage = get_sub_field('mkp_brand_page');
				$binfo = get_sub_field('mkp_brand_info');
                $bclr = get_sub_field('mkp_brand_info_clr');
				$bg = get_sub_field('mkp_brand_background_image');

				if ( $i == 0 ){
					$class = 'active';
				} else {
					$class='';
				}

				?>
				<div class="item <?php echo $class; ?>" style="background-image: url(<?php echo $bg['url']; ?>);">
					<div class="container">
						<div class="row flex--justify-right">
							<div class="col-nano-12 col-md-4">
						      <div class="brand-info text-right text-sm-center color-<?php echo $bclr; ?>">
                                <?php echo $binfo; ?>
                                <a class="btn btn-default btn-cta" href="<?php echo $bpage; ?>">learn more</a>
                              </div>
							</div>
						</div>
					</div>
				</div>
				<?php endwhile;
				endif;
				?>
			</div><!-- /.carousel-inner -->
		</div><!-- /#carousel-hero -->
    </section>

    <section id="section--search-bar" class="offSet">
      <?php get_template_part('templates/include', 'search'); ?>
    </section>

    <section id="section--trending-brands" class="section--pad">
        <div class="container">
            <!-- TITLE ROW -->
            <div class="row">
                <div class="col-nano-12">
                    <div class="text-sm-left text-upper title--pad">
                        <?php the_field('mkp_brand_list_section'); ?>
                    </div>
                </div>
            </div>

            <!-- CONTENT ROW -->
            <div class="row card_rowSpaceL">
                <?php wp_reset_postdata();

                $the_query = new WP_Query( ['post_type' => 'mgl_brand', 'orderby'=>'rand', 'meta_key' => '_mgl_brand_is_trending', 'posts_per_page' => '6'] );

                if($the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    ?>

                    <div class="col-nano-12 col-sm-6 col-lg-4 card-grid">
                        
                            <div class="card h-100">
								<h3 style="display:inline-block"><a href="<?php echo get_the_permalink() ?>"><?php the_title() ?></a>
								<span class="category">
                              	<?php foreach(Brand\get_brand_categories(get_the_ID()) as $brand): ?>
									<?php if(!$brand->parent): ?>
										<a href="/marketplace?category=<?= $brand->slug ?>">
										  <span class="badge catIcon"><img src="<?= get_template_directory_uri() ?>/dist/images/cat_icon-<?= $brand->name ?>.svg" alt="<?= $brand->name ?>" title="<?= $brand->name ?>"></span>
										</a>
									<?php endif; ?>
								<?php endforeach; ?>
								</span>
								</h3>
								<p>
                                    <?php echo Brand\brand_impact(get_the_ID()) ?>
                                </p>
                            </div>
                       
                    </div>

                <?php endwhile; ?>
                <?php endif; wp_reset_postdata(); ?>

            </div>

            <div class="row view-more visible-xs" id="viewBrands">
                <div class="col-nano-12 text-center">
                    <a class="btn-view-more text-upper" href="#view-more-brands">View More</a>
                </div>
            </div>

        </div>
    </section>

    <hr>

    <section id="section--popular-products" class="section--pad">
        <div class="container">
            <!-- TITLE ROW -->
            <div class="row">
                <div class="col-nano-12">
                    <div class="text-sm-left text-upper title--pad">
                        <?php the_field('mkp_product_list_section'); ?>
                    </div>
                </div>
            </div>

          <?php get_template_part('templates/include', 'filter'); ?>

            <div style="height: auto!important;" class="row masonry-container" id="brand_marketplace_container">
                <div id="ajax-space" class="col-nano-12 col-micro-6 col-xs-6 col-sm-6 col-md-4 col-lg-3 masonry-sizer"></div><!-- left empty for column sizing -->

                <?php wp_reset_postdata();

                $the_query = new WP_Query( ['post_type' => 'mgl_product', 'posts_per_page' => 60, 'orderby' => 'rand'] );

                if($the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    ?>

                    <div class="col-lg-3 col-md-4 col-nano-6 masonry-item">
                        <div class="thumbnail">
                            <img src="<?php echo Product\get_thumbnail(get_the_ID()) ?>" class="">
                            <div class="caption">
                                <p class="brand">
                                    <?php echo Product\get_brand_title(get_the_ID()) ?>
                                </p>
                                <h3><?php the_title() ?></h3>
                                <p class="price"><?= Product\get_price(get_the_ID()) ?></p>
                            </div>
                            <a href="<?php echo get_the_permalink() ?>"><div class="mask"></div></a>
                        </div>
                    </div>
                <?php endwhile;  ?>
                <?php endif; wp_reset_postdata();  ?>
            </div>

            <div class="row view-more" id="viewProducts">
                <div class="col-nano-12 text-center">
                    <a style="margin-bottom: 50px;" id="load-more-posts" href="<?php echo site_url('/view-more') ?>" class="btn-view-more text-upper" >View More</a>
                </div>
            </div>

        </div>
    </section>

</div>

<script type="text/javascript">

    jQuery(document).ready(function() {
      
        //Carousel 
        jQuery('.carousel').carousel({
          interval: 5000,
          pause: "hover",
          wrap: true
        })

        //var category = document.getElementById('category-select');
        //var subcategory = document.getElementById('persona-select');
		var first = true;
        var firstLoad = true;
        //jQuery(category).on('change', function () {
        //    firstLoad = true;
        //    getPosts();
        //});
        //jQuery(subcategory).on('change', function () {
		//firstLoad = true;
        //   getPosts();
        //});
		jQuery(".select-items").on('click', function () {
            firstLoad = true;
            getPosts();
        });

        /**
         *
         jQuery("#load-more-posts").on('click', function (e) {

            firstLoad = false;
            getPosts();
        });
         */

        masonry2();
        first = false;
        function getPosts(){
            //var cat = jQuery("#category-select option:selected").val();
            //var persona = jQuery("#persona-select option:selected").val();
			var cat = jQuery(".category-select").attr("value");
            var persona = jQuery(".persona-select").attr("value");
            var scope = '';
			
			if (persona !== '' && cat !== '' && cat !== 'all')
                scope = persona + '-' + cat;
            else if(persona === '')
                scope = cat;
            else if(persona === '' && cat === '')
                scope = 'all';
            else
                scope = persona;

            if (scope === 'undefined' || scope === 'undefined-undefined')
                scope = '';
			
            jQuery.when(
                jQuery.ajax({
                    url: '/getDirectoryPosts.php?scope="' + scope + '"',
                    success: function(response){
                        var data = jQuery.parseJSON(response);

                        var container = jQuery("#brand_marketplace_container");
                       if (firstLoad)
                       {
                           container.html('<div id="ajax-space" class="col-nano-12 col-micro-6 col-xs-6 col-sm-6 col-md-4 col-lg-3 masonry-sizer"></div>');
                           container.css({'height' : 'auto'});
                       }
                        console.log(data);
                        jQuery.each(data, function (key, product)
                        {

                            var item = '<div class="col-lg-3 col-md-4 col-nano-6 masonry-item">'+
                                '<div class="thumbnail">'+
                                '<img src="' + product.thumbnail + '" class="">'+
                                '<div class="caption">'+
                                '<p class="brand">'+
                                product.brand +
                                '</p>'+

                                '<h3>' +
                                product.title +
                                '</h3>' +
                                '<p class="price">' +
                                product.price +
                                '</p>'+
                                '</div>' +
                                '<a href="' + product.url + '"><div class="mask"></div></a>' +
                                '</div>' +
                                '</div>';

                            container.append(item);

                        });


                    }
                })
            ).then(masonry2);



        }

        function masonry2(){
            var masonry = jQuery('#brand_marketplace_container');

            jQuery(masonry).imagesLoaded( function() {
                jQuery(masonry).masonry({
                    columnWidth: '.masonry-sizer',
                    itemSelector: '.masonry-item',
                    percentPosition: true
                });
            });
            if (!first)
            {
                jQuery(masonry).masonry('reload');
                jQuery(masonry).masonry('layout');
            }

        }
    });

</script>
