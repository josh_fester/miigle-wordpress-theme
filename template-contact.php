<?php
/**
 * Template Name: Contact
 */
?>

<div id="template-contact">

  <section id="splash" class="text-center page-splash">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
        
          <h1><span class="text-navy">Say</span> Hello.</h1>
          
        </div>
      </div>
    </div>
  </section>

  <?php while (have_posts()) : the_post(); ?>
  <section id="contact-form">
    <div class="container">
      <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
          <div class="well">
            <?php
                $getPost = get_the_content();
                $postwithbreaks = wpautop( $getPost, true );
                $withAdditional = apply_filters('the_content', $postwithbreaks);
                echo $withAdditional;
             ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php endwhile; ?>

</div>