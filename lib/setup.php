<?php

namespace Roots\Sage\Setup;

/**
 * Theme setup
 */
function setup() {
	// Enable features from Soil when plugin is activated
	// https://roots.io/plugins/soil/
	add_theme_support( 'soil-clean-up' );
	add_theme_support( 'soil-nav-walker' );
	add_theme_support( 'soil-nice-search' );
	add_theme_support( 'soil-jquery-cdn' );
	add_theme_support( 'soil-relative-urls' );

	// Make theme available for translation
	// Community translations can be found at https://github.com/roots/sage-translations
	load_theme_textdomain( 'sage', get_template_directory() . '/lang' );

	// Enable plugins to manage the document title
	// http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
	add_theme_support( 'title-tag' );

	// Register wp_nav_menu() menus
	// http://codex.wordpress.org/Function_Reference/register_nav_menus
	register_nav_menus( [
		'primary_navigation_featured' => __( 'Primary Navigation (Featured)', 'sage' )
	] );

  register_nav_menus( [
    'primary_navigation_collapsed' => __( 'Primary Navigation (Collapsed)', 'sage' )
  ] );

	// Enable post thumbnails
	// http://codex.wordpress.org/Post_Thumbnails
	// http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
	// http://codex.wordpress.org/Function_Reference/add_image_size
	add_theme_support( 'post-thumbnails' );

	// Enable post formats
	// http://codex.wordpress.org/Post_Formats
	add_theme_support( 'post-formats', [ 'aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio' ] );

	// Enable HTML5 markup support
	// http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
	add_theme_support( 'html5', [ 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ] );

	// Use main stylesheet for visual editor
	// To add custom styles edit /assets/styles/layouts/_tinymce.scss
	//add_editor_style(Assets\asset_path('styles/main.css'));
}

add_action( 'after_setup_theme', __NAMESPACE__ . '\\setup' );

// hide that nasty toolbar
add_filter( 'show_admin_bar', '__return_false' );

/**
 * Add <body> classes
 */
function body_class( $classes ) {
	// Add page slug if it doesn't exist
	if ( is_single() || is_page() && ! is_front_page() ) {
		if ( ! in_array( basename( get_permalink() ), $classes ) ) {
			$classes[] = basename( get_permalink() );
		}
	}

  if ( is_page_template( 'template-press.php' ) ) {
    $classes[] = 'body';
  }
	return $classes;
}

add_filter( 'body_class', __NAMESPACE__ . '\\body_class' );

/**
 * Theme assets
 */
function assets() {
  wp_register_style( 'bootstrap/css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css', false, null );
	// wp_register_style( 'bootstrap/css', get_template_directory_uri() . '/dist/styles/bootstrap-custom.css', null, '1.0.5' );
  wp_enqueue_style( 'bootstrap/css' );
  wp_register_style( 'fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', false, null );
  wp_register_style( 'googlefonts', 'https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700|Open+Sans:300,400,600,700,300italic,400italic|Lato', false, null );

  wp_register_script( 'validate', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js', [ 'jquery' ], null, true );
  wp_register_script( 'jquery-cookie', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js', [ 'jquery' ], null, true );
  wp_register_script( 'bootstrap/js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js', [ 'jquery' ], null, true );
  wp_enqueue_script( 'bootstrap/js' );
  wp_register_script( 'masonry', 'https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js', [ 'jquery' ], null, true );
  wp_register_script( 'imagesloaded', 'https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js', [ 'jquery' ], null, true );

  if ( is_page_template( 'template-landing_page.php' ) ) {
    wp_enqueue_style( 'landing/css', get_template_directory_uri() . '/dist/styles/landing.css', null, '1.0.12' );
    wp_enqueue_script( 'landing/js', get_template_directory_uri() . '/dist/scripts/landing.js', [ 'jquery' ], '1.0.9', true );
  } else {
		wp_enqueue_style( 'sage/css', get_template_directory_uri() . '/dist/styles/main.css', [
			'bootstrap/css',
			'fontawesome',
			'googlefonts'
		], '0.2.2' );
	  	wp_enqueue_style( 'grid/css', get_template_directory_uri() . '/assets/styles/grid.css', null, '1.1.4' );
	  	wp_enqueue_style( 'marketplace/css', get_template_directory_uri() . '/assets/styles/marketplace.css', null, '2.5.2' );
		wp_enqueue_script( 'sage/js', get_template_directory_uri() . '/assets/scripts/main.js', [
			'jquery',
			'validate',
			'bootstrap/js',
			'masonry',
			'wp-api',
			'braintree/fields'
		], '1.4.5', true );
	}

	if ( is_single() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script( 'dotdotdot', get_template_directory_uri() . '/assets/scripts/jquery.dotdotdot.min.js', [
		'jquery',
		'validate',
		'bootstrap/js',
		'wp-api'
	], null, true );

	if ( is_page_template( is_page_template( 'template-brand_submit.php' ) ) ) {
		wp_enqueue_script( 'braintree', 'https://js.braintreegateway.com/web/3.6.0/js/client.min.js', [ 'jquery' ], null, true );
		wp_enqueue_script( 'braintree/fields', 'https://js.braintreegateway.com/web/3.6.0/js/hosted-fields.min.js', [ 'braintree' ], null, true );
		wp_enqueue_script( 'braintree/paypal', '//js.braintreegateway.com/web/3.6.0/js/paypal.min.js', [ 'braintree' ], null, true );
	}

  if ( is_page_template( 'template-press.php' ) ) {
    // wp_deregister_script( 'jquery' );
    wp_enqueue_style( 'press/css', get_template_directory_uri() . '/assets/styles/press.css', null, '0.0.5' );
    wp_enqueue_script( 'press/jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js', [], '0.0.4', true );
    wp_enqueue_script( 'press/js', get_template_directory_uri() . '/assets/scripts/press.js', [ 'press/jquery' ], '0.0.4', true );
    remove_filter( 'the_excerpt', 'wpautop' );
  }
}

add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100 );

function exclude_press_from_posts( $query ) {
  if ( is_admin() || ! $query->is_main_query() )
    return;

  if ( $query->is_posts_page ) {
    $cat = get_category_by_slug('press');
    $query->set( 'cat', '-' . $cat->term_id );
  }
}
add_action( 'pre_get_posts', __NAMESPACE__ . '\\exclude_press_from_posts', 9999 );

/*  Add responsive container to embeds
/* ------------------------------------ */ 
function contain_embed_html( $html ) {
    return '<div class="miigle-video w-embed w-video media full">' . $html . '</div>';
}
 
add_filter( 'embed_oembed_html', __NAMESPACE__ . '\\contain_embed_html', 10, 3 );
add_filter( 'video_embed_html', __NAMESPACE__ . '\\contain_embed_html' ); // Jetpack
