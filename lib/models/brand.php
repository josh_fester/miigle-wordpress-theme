<?php

namespace Miigle\Models\Brand;

use Miigle\Models\Model;
use Miigle\Models\Product;

add_action( 'init', __NAMESPACE__ . '\\register' );
add_action( 'cmb2_admin_init', __NAMESPACE__ . '\\register_meta' );
add_action( 'rest_api_init', __NAMESPACE__ . '\\api_register_meta' );
add_action( 'pre_get_posts', __NAMESPACE__ . '\\pre_get_posts_brands' );

/**
 * Produt Post Type
 */
function register() {
	$args = array(
		'public'       => true,
		'label'        => 'Brands',
		'has_archive'  => true,
		'rewrite'      => array( 'slug' => 'brands' ),
		'show_in_rest' => true,
		'rest_base'    => 'brands',
		'supports'     => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
	);
	register_post_type( 'mgl_brand', $args );

	register_taxonomy(
		'mgl_brand_badge',
		'mgl_brand',
		array(
			'label'        => __( 'Badges' ),
			'rewrite'      => array( 'slug' => 'badge' ),
			'show_in_rest' => true,
			'rest_base'    => 'brands-badges',
			'hierarchical' => true,
		)
	);
}

/**
 * Define the metabox and field configurations.
 */
function register_meta() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_mgl_brand_';

	/**
	 * Initiate the metabox
	 */
	$cmb = new_cmb2_box( array(
		'id'           => $prefix,
		'title'        => __( 'Brand Fields', 'cmb2' ),
		'object_types' => array( 'mgl_brand' ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );

	// URL
	$cmb->add_field( array(
		'name' => __( 'Tracking URL', 'cmb2' ),
		'desc' => __( '', 'cmb2' ),
		'id'   => $prefix . 'url',
		'type' => 'text_url'
	) );

	// Destination URL
	$cmb->add_field( array(
		'name' => __( 'Full URL', 'cmb2' ),
		'desc' => __( '', 'cmb2' ),
		'id'   => $prefix . 'original_url',
		'type' => 'text_url'
	) );

	// Directory Listing
	$cmb->add_field( array(
		'name' => __( 'Show in Directory Listing?', 'cmb2' ),
		'desc' => __( 'YES', 'cmb2' ),
		'id'   => $prefix . 'brand_show',
		'type' => 'checkbox'
	) );

	// Is Paid
	$cmb->add_field( array(
		'name' => __( 'Is Paid?', 'cmb2' ),
		'desc' => __( 'YES (Badge apply)', 'cmb2' ),
		'id'   => $prefix . 'is_paid',
		'type' => 'checkbox'
	) );

	// Is Sponsored
	$cmb->add_field( array(
		'name' => __( 'Is Sponsored?', 'cmb2' ),
		'desc' => __( 'YES', 'cmb2' ),
		'id'   => $prefix . 'is_sponsored',
		'type' => 'checkbox'
	) );

	// Is Trending
	$cmb->add_field( array(
		'name' => __( 'Is Trending?', 'cmb2' ),
		'desc' => __( 'YES', 'cmb2' ),
		'id'   => $prefix . 'is_trending',
		'type' => 'checkbox'
	) );

	// CTA button
	$cmb->add_field( array(
		'name'             => __( 'Select Button', 'cmb2' ),
		'desc'             => __( '', 'cmb2' ),
		'id'               => $prefix . 'brand_cta',
		'type'             => 'select',
		'show_option_none' => false,
		'default'          => 'claim',
		'options'          => array(
			'claim'      => __( 'CLAIM THIS and VISIT WEBSITE', 'cmb2' ),
			'claim_only' => __( 'CLAIM THIS', 'cmb2' ),
			'website'    => __( 'VISIT WEBSITE', 'cmb2' ),
			'both'       => __( 'Both', 'cmb2' )
		),
	) );

	// Social Impact
	$cmb->add_field( array(
		'name' => __( 'Social Impact Text', 'cmb2' ),
		'desc' => __( 'Max 140 characters', 'cmb2' ),
		'id'   => $prefix . 'brand_impact',
		'type' => 'textarea'
	) );

	// Brand description
	$cmb->add_field( array(
		'name' => __( 'Brand Description Text', 'cmb2' ),
		'desc' => __( '', 'cmb2' ),
		'id'   => $prefix . 'brand_description',
		'type' => 'textarea'
	) );
	
	// Brand Quote
	$cmb->add_field( array(
		'name' => __( 'Brand Quote', 'cmb2' ),
		'desc' => __( '', 'cmb2' ),
		'id'   => $prefix . 'brand_quote',
		'type' => 'textarea'
	) );
	
	// Brand Main Image
	$cmb->add_field( array(
		'name' => __( 'Brand Main Image', 'cmb2' ),
		'desc' => __( '', 'cmb2' ),
		'id'   => $prefix . 'brand_main_img',
		'type' => 'file'
	) );
	
	// Brand Video
	$cmb->add_field( array(
		'name' => __( 'Brand Video', 'cmb2' ),
		'desc' => __( '', 'cmb2' ),
		'id'   => $prefix . 'brand_video',
		'type' => 'textarea'
	) );
  
    // Is Brand Information Coming Soon
	$cmb->add_field( array(
		'name' => __( 'Brand Information Not Completed?', 'cmb2' ),
		'desc' => __( 'YES', 'cmb2' ),
		'id'   => $prefix . 'has_info',
		'type' => 'checkbox'
	) );
  
    // Is Product Coming Soon
	$cmb->add_field( array(
		'name' => __( 'Product Coming Soon?', 'cmb2' ),
		'desc' => __( 'YES', 'cmb2' ),
		'id'   => $prefix . 'has_product',
		'type' => 'checkbox'
	) );
  
    // Coming Soon Text
	$cmb->add_field( array(
		'name' => __( 'Coming Soon Text', 'cmb2' ),
		'desc' => __( '', 'cmb2' ),
		'id'   => $prefix . 'brand_coming_soon',
		'type' => 'wysiwyg'
	) );
	

}

function api_register_meta() {
	$prefix = '_mgl_brand_';

	register_rest_field(
		'mgl_brand',
		$prefix . 'url',
		array(
			'get_callback'    => 'Miigle\\Models\\Model\\get_miigle_slug',
			'update_callback' => 'Miigle\\Models\\Model\\update_miigle_slug',
			'schema'          => null,
		)
	);

	register_rest_field(
		'mgl_brand',
		$prefix . 'original_url',
		array(
			'get_callback'    => 'Miigle\\Models\\Model\\get_miigle_slug',
			'update_callback' => 'Miigle\\Models\\Model\\update_miigle_slug',
			'schema'          => null,
		)
	);

	register_rest_field(
		'mgl_brand',
		$prefix . 'brand_show',
		array(
			'get_callback'    => 'Miigle\\Models\\Model\\get_miigle_slug_checkbox',
			'update_callback' => null,
			'schema'          => null,
		)
	);

	register_rest_field(
		'mgl_brand',
		$prefix . 'is_paid',
		array(
			'get_callback'    => 'Miigle\\Models\\Model\\get_miigle_slug_checkbox',
			'update_callback' => null,
			'schema'          => null,
		)
	);

	register_rest_field(
		'mgl_brand',
		$prefix . 'is_sponsored',
		array(
			'get_callback'    => 'Miigle\\Models\\Model\\get_miigle_slug_checkbox',
			'update_callback' => null,
			'schema'          => null,
		)
	);

	register_rest_field(
		'mgl_brand',
		$prefix . 'is_trending',
		array(
			'get_callback'    => 'Miigle\\Models\\Model\\get_miigle_slug_checkbox',
			'update_callback' => null,
			'schema'          => null,
		)
	);

	register_rest_field(
		'mgl_brand',
		$prefix . 'brand_cta',
		array(
			'get_callback'    => 'Miigle\\Models\\Model\\get_miigle_slug',
			'update_callback' => null,
			'schema'          => null,
		)
	);

	register_rest_field(
		'mgl_brand',
		$prefix . 'brand_impact',
		array(
			'get_callback'    => 'Miigle\\Models\\Model\\get_miigle_slug',
			'update_callback' => null,
			'schema'          => null,
		)
	);

	register_rest_field(
		'mgl_brand',
		$prefix . 'brand_description',
		array(
			'get_callback'    => 'Miigle\\Models\\Model\\get_miigle_slug',
			'update_callback' => null,
			'schema'          => null,
		)
	);

	register_rest_field(
		'mgl_brand',
		'product_count',
		array(
			'get_callback'    => __NAMESPACE__ . '\\get_product_count',
			'update_callback' => null,
			'schema'          => null,
		)
	);
  
    register_rest_field(
		'mgl_brand',
		$prefix . 'has_info',
		array(
			'get_callback'    => 'Miigle\\Models\\Model\\get_miigle_slug_checkbox',
			'update_callback' => null,
			'schema'          => null,
		)
	);
  
    register_rest_field(
		'mgl_brand',
		$prefix . 'has_product',
		array(
			'get_callback'    => 'Miigle\\Models\\Model\\get_miigle_slug_checkbox',
			'update_callback' => null,
			'schema'          => null,
		)
	);
}

/**
 * Modify the archive query
 */
function pre_get_posts_brands( $query ) {
	$is_brands = ( isset( $_GET['brands'] ) || is_post_type_archive( 'mgl_brand' ) );

	if ( ! $is_brands ) {
		return;
	}

	// the tax pages must have either ?products or ?brands
	if ( is_tax( 'mgl_product_category' ) ) {
		if ($query->get('post_type') !== 'nav_menu_item') {
      $query->set( 'post_type', 'mgl_brand' );
    }
	}

	// popular sort
	if ( isset( $_GET['sort'] ) && $_GET['sort'] == 'popular' ) {

		$query->set( 'meta_query', array(
			//'relation' => 'OR',
			'upvotes_clause' => array(
				'key'     => '_mgl_brand_upvotes',
				'compare' => 'EXISTS',
				//'type' => 'NUMERIC'
			)
		) );

		$query->set( 'orderby', array(
			'upvotes_clause' => 'DESC'
		) );

	}

}

/**
 * Get all products posted by a user
 */
function get_posts() {
	$query = new \WP_Query( array(
		'post_type'      => 'mgl_brand',
		'post_status'    => 'publish',
		'posts_per_page' => - 1
	) );

	wp_reset_postdata();

	return $query;
}

/**
 * Get the badges for a brand
 */
function get_badges( $post_id ) {
	return wp_get_post_terms( $post_id, 'mgl_brand_badge' );
}

/**
 * Get the product upvotes
 */
function get_upvotes( $post_id ) {
	return intval( get_post_meta( $post_id, '_mgl_brand_upvotes', true ) );
}

/**
 * Get the main image
 */
function get_main_img( $post_id ) {
    return get_post_meta( $post_id, '_mgl_brand_brand_main_img', true );
}

/**
 * Get the users who have upvoted the product
 */
function get_upvotes_users( $post_id ) {
	return get_post_meta( $post_id, '_mgl_brand_upvotes_users', true );
}

/**
 * Upvote
 */
function upvote( $post_id, $user_id ) {
	return Model\upvote( $post_id, $user_id, 'mgl_brand', '_mgl_brand' );
}

/**
 * Downvote
 */
function downvote( $post_id, $user_id ) {
	return Model\downvote( $post_id, $user_id, 'mgl_brand', '_mgl_brand' );
}

/**
 * Get the url
 */
function get_url( $post_id ) {
	return get_post_meta( $post_id, '_mgl_brand_url', true );
}
/**
 * Get the quote
 */
function get_quote( $post_id ) {
	return get_post_meta( $post_id, '_mgl_brand_brand_quote', true );
}

/**
 * Get the coming soon
 */
function get_coming_soon( $post_id ) {
	return get_post_meta( $post_id, '_mgl_brand_brand_coming_soon', true );
}

/**
 * Get brand_show
 */
function brand_show( $post_id ) {
	if ( get_post_meta( $post_id, '_mgl_brand_brand_show', true ) ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Get is_paid
 */
function is_paid( $post_id ) {
	if ( get_post_meta( $post_id, '_mgl_brand_is_paid', true ) ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Get has_info
 */
function has_info( $post_id ) {
	if ( get_post_meta( $post_id, '_mgl_brand_has_info', true ) ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Get has_product
 */
function has_product( $post_id ) {
	if ( get_post_meta( $post_id, '_mgl_brand_has_product', true ) ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Get is_sponsored
 */
function is_sponsored( $post_id ) {
	if ( get_post_meta( $post_id, '_mgl_brand_is_sponsored', true ) ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Get brand_cta
 */
function brand_cta( $post_id ) {
	return get_post_meta( $post_id, '_mgl_brand_brand_cta', true );
}

/**
 * Get brand_impact
 */
function brand_impact( $post_id ) {
	return get_post_meta( $post_id, '_mgl_brand_brand_impact', true );
}

/**
 * Get brand_description
 */
function brand_description( $post_id ) {
	return get_post_meta( $post_id, '_mgl_brand_brand_description', true );
}

/**
 * Get brand categories
 */
function get_brand_categories( $post_id = false, $hide_empty = true ) {
	if ( $post_id ) {
		return wp_get_post_terms( $post_id, 'mgl_product_category' );
	} else {
		return get_terms( array(
			'taxonomy'   => 'mgl_product_category',
			'hide_empty' => $hide_empty,
		) );
	}
}

/**
 * Grab brands limited by from and to first character!
 *
 * @param array $data Options for the function.
 *
 * @return array|string Brands
 */
function get_filtered_brands( $from, $to ) {
	trigger_error( "Deprecated function called.", E_USER_DEPRECATED );

	global $wpdb;

	$postids = $wpdb->get_col( $wpdb->prepare( "
	SELECT      ID
	FROM        $wpdb->posts
	WHERE       SUBSTR($wpdb->posts.post_title,1,1) >= %s
	AND         SUBSTR($wpdb->posts.post_title,1,1) <= %s
	AND 		$wpdb->posts.post_type = 'mgl_brand'
	ORDER BY    $wpdb->posts.post_title", $from, $to ) );

	$posts_array = [];
	if ( $postids ) {
		$args        = array(
			'post__in'       => $postids,
			'post_type'      => 'mgl_brand',
			'post_status'    => 'publish',
			'posts_per_page' => - 1,
			'orderby'        => 'title',
			'order'          => 'ASC',
			'meta_key'       => '_mgl_brand_is_paid',
			'meta_value'     => 'on',
		);
		$posts_array = \get_posts( $args );

		wp_reset_query();
	}

	return $posts_array;
}

/**
 * Get random brand product thumbnails
 *
 * @param $id
 * @param $args
 *
 * @return array
 */
function get_brand_thumbnails( $id, $args ) {
	$defaults = array(
		'count' => 4,
	);

	$args = wp_parse_args( $args, $defaults );

	$query = new \WP_Query( array(
		'post_type'      => 'mgl_product',
		'posts_per_page' => $args['count'],
		'meta_key'       => '_mgl_product_brand_id',
		'meta_value'     => $id,
		'fields'         => 'ids',
		'orderby'        => 'rand',
	) );

	$thumbs_array = [];
	foreach ( $query->posts as $product_id ) {
		if ( $thumb = Product\get_thumbnail( $product_id, '_mgl_product_image_gallery', null, null ) ) {
			$thumbs_array[] = $thumb;
		}
	}

	wp_reset_postdata();

	return $thumbs_array;
}

/**
 * Get the value of the "slug" field
 *
 * @param array $object Details of current post.
 * @param string $field_name Name of field.
 * @param WP_REST_Request $request Current request
 *
 * @return mixed
 */
function get_product_count( $object, $field_name, $request ) {
	$query = new \WP_Query( array(
		'post_type'  => 'mgl_product',
		'nopaging'   => true,
		'meta_key'   => '_mgl_product_brand_id',
		'meta_value' => $object['id'],
		'fields'     => 'ids',
	) );

	return $query->post_count;
}
