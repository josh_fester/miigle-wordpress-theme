<?php

namespace Miigle\Models\Subscription;

use Miigle\Models\Model;

add_action('init', __NAMESPACE__ . '\\register');
add_action('cmb2_admin_init', __NAMESPACE__ . '\\register_meta');

/**
 * Produt Post Type
 */
function register() {
  $args = array(
    'public'      => true,
    'label'       => 'Subscriptions',
    'has_archive' => true,
    'rewrite'     => array('slug' => 'subscriptions'),
    'show_in_rest'=> true,
    'supports'    => array('title', 'editor', 'author', 'thumbnail', 'excerpt')
  );
  register_post_type('mgl_subscription', $args);
}

/**
 * Define the metabox and field configurations.
 */
function register_meta() {

  // Start with an underscore to hide fields from custom fields list
  $prefix = '_mgl_subscription_';

  /**
    * Initiate the metabox
    */
  $cmb = new_cmb2_box(array(
    'id'            => $prefix,
    'title'         => __('Subscription Fields', 'cmb2'),
    'object_types'  => array('mgl_subscription'), // Post type
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    // 'cmb_styles' => false, // false to disable the CMB stylesheet
    // 'closed'     => true, // Keep the metabox closed by default
  ));
  
  // Is Paid
  $cmb->add_field(array(
    'name'       => __('Paid?', 'cmb2'),
    'desc'       => __('', 'cmb2'),
    'id'         => $prefix . 'is_paid',
    'type'       => 'checkbox'
  ));

  // Date Paid
  $cmb->add_field(array(
    'name'       => __('First Billing Date', 'cmb2'),
    'desc'       => __('', 'cmb2'),
    'id'         => $prefix . 'first_billing_date',
    'type'       => 'text_date'
  ));

  // Subscription Type
  $cmb->add_field(array(
    'name'       => __('Type', 'cmb2'),
    'desc'       => __('', 'cmb2'),
    'id'         => $prefix . 'type',
    'type'             => 'select',
    'show_option_none' => true,
    'options'          => array(
        '1month' => __( '1 Month', 'cmb2' ),
        '3month' => __( '3 Month', 'cmb2' ),
        '6month' => __( '6 Month', 'cmb2' ),
        '12month' => __( '12 Month', 'cmb2' )
    ),
  ));

  // User ID
  $cmb->add_field(array(
    'name'       => __('User ID', 'cmb2'),
    'desc'       => __('', 'cmb2'),
    'id'         => $prefix . 'user_id',
    'type'       => 'text'
  ));

  // Phone
  $cmb->add_field(array(
    'name'       => __('Phone', 'cmb2'),
    'desc'       => __('', 'cmb2'),
    'id'         => $prefix . 'phone',
    'type'       => 'text'
  ));

  // Website
  $cmb->add_field(array(
    'name'       => __('Website', 'cmb2'),
    'desc'       => __('', 'cmb2'),
    'id'         => $prefix . 'website',
    'type'       => 'text'
  ));

  // Impact
  $cmb->add_field(array(
    'name'       => __('Impact', 'cmb2'),
    'desc'       => __('', 'cmb2'),
    'id'         => $prefix . 'impact',
    'type'       => 'text'
  ));

  // Brand Name
  $cmb->add_field(array(
    'name'       => __('Brand Name', 'cmb2'),
    'desc'       => __('', 'cmb2'),
    'id'         => $prefix . 'brand_name',
    'type'       => 'text'
  ));

  // Brand ID
  $cmb->add_field(array(
    'name'       => __('Brand ID', 'cmb2'),
    'desc'       => __('', 'cmb2'),
    'id'         => $prefix . 'brand_id',
    'type'       => 'text'
  ));

  // Braintree ID
  $cmb->add_field(array(
    'name'       => __('Braintree Subscription ID', 'cmb2'),
    'desc'       => __('', 'cmb2'),
    'id'         => $prefix . 'braintree_id',
    'type'       => 'text'
  ));

  // Braintree User ID
  $cmb->add_field(array(
    'name'       => __('Braintree User ID', 'cmb2'),
    'desc'       => __('', 'cmb2'),
    'id'         => $prefix . 'braintree_user_id',
    'type'       => 'text'
  ));
  
}