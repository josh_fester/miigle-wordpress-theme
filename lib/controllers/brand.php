<?php

namespace Miigle\Controllers\Brand;

use Miigle\Models\Brand;

/**
 * @param $data
 *
 * @return array|string
 */
function filter( $data ) {
	return Brand\get_filtered_brands( $data['from'], $data['to'] );
}

/**
 * @param $data
 *
 * @return array
 */
function get_brand_thumbnails( $data ) {
	return Brand\get_brand_thumbnails( $data['id'], $data->get_params() );
}