<?php

namespace Miigle\Controllers\Subscription;

use Miigle\Controllers\User;

/**
 * Return all braintree plans
 *
 * @return array id => price
 */
function index() {
	$plans = \Braintree_Plan::all();

	$result = [];
	foreach ( $plans as $plan ) {
		$result[ $plan->id ] = array(
			'name'  => $plan->name,
			'price' => $plan->price,
		);
	}

	uasort( $result, function ( $a, $b ) {
		return $a['price'] - $b['price'];
	} );

	return $result;
}


/**
 * Check validity of a given promo code and return its value.
 *
 * @param $request promo-code path parameter
 *
 * @return array|bool Braintree_Discount
 */
function discount( $request ) {
	$code = $request->get_param( 'code' );

	$discounts = \Braintree_Discount::all();

	foreach ( $discounts as $discount ) {
		if ( $discount->id == $code ) {
			return array(
				'id'          => $discount->id,
				'name'        => $discount->name,
				'description' => $discount->description,
				'amount'      => $discount->amount,
			);
		}
	}

	return false;
}

/**
 * Creates a Subscription
 */
function post( $data ) {
	// first create a user
	$user = User\post( $data );

	if ( ! $user ) {
		return 'error: no user found for email ' . $data['email'];
	}

	$customer_result = \Braintree_Customer::create( [
		'firstName'          => $data['firstName'],
		'lastName'           => $data['lastName'],
		'company'            => $data['brandName'],
		'paymentMethodNonce' => $data['payment-method-nonce']
	] );

	if ( ! $customer_result->success ) {
		return 'error: could not create braintree customer';
	}

	$subs_args = [
		'paymentMethodToken' => $customer_result->customer->paymentMethods[0]->token,
		'planId'             => $data['plan'],
	];
	if ( $data['promo'] != null && $data['promo'] != '' ) {
		$subs_args['discounts'] = [
			'add' => [
				[
					'inheritedFromId' => $data['promo'],
				],
			]
		];
	}

	$subscription_result = \Braintree_Subscription::create( $subs_args );

	$new_post = array(
		'post_title'  => $data['email'],
		'post_status' => 'publish',
		'post_date'   => date( 'Y-m-d H:i:s' ),
		'post_author' => $user,
		'post_type'   => 'mgl_subscription'
	);

	$post_id = wp_insert_post( $new_post );

	add_post_meta( $post_id, '_mgl_subscription_is_paid', true, true );
	add_post_meta( $post_id, '_mgl_subscription_phone', $data['phone'], true );
	add_post_meta( $post_id, '_mgl_subscription_website', $data['website'], true );
	add_post_meta( $post_id, '_mgl_subscription_brand_name', $data['brandName'], true );
	add_post_meta( $post_id, '_mgl_subscription_impact', $data['impact'], true );
	add_post_meta( $post_id, '_mgl_subscription_first_billing_date', $subscription_result->subscription->firstBillingDate->format( 'Y-m-d H:i:s' ), true );
	add_post_meta( $post_id, '_mgl_subscription_type', $data['plan'], true );
	add_post_meta( $post_id, '_mgl_subscription_user_id', $user, true );
	add_post_meta( $post_id, '_mgl_subscription_braintree_id', $subscription_result->subscription->id, true );
	add_post_meta( $post_id, '_mgl_subscription_braintree_user_id', $customer_result->customer->id, true );

	if ( $data['mgl_product_category'] ) {
		$categories = array();

		foreach ( $data['mgl_product_category'] as $cat ) {
			array_push( $categories, intval( $cat ) );
		}

		wp_set_object_terms( $post_id, $categories, 'mgl_product_category' );
	}

	wp_mail(
		'luc.berlin@miigle.com',
		'Miigle - New Subscription',
		$data['email'] . ' has subscribed to ' . $data['plan']
	);

	return $post_id;
}