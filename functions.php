<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'vendor/braintree/lib/Braintree.php', // Braintree SDK
  'lib/braintree.php',            // Braintree config
  'lib/models/model.php',         // Shared model functions
  'lib/models/product.php',       // Product post type
  'lib/models/role-request.php',  // Role-request post type
  'lib/models/brand.php',         // Brand post type
  'lib/models/user.php',          // User meta
  'lib/models/subscription.php',  // Subscription post type
  'lib/controllers/application.php',
  'lib/controllers/brand.php',
  'lib/controllers/product.php',
  'lib/controllers/subscription.php',
  'lib/controllers/user.php',
  'lib/rest-config.php',
  'lib/setup.php',                // Theme setup
  'lib/titles.php',               // Page titles
  'lib/wrapper.php',              // Theme wrapper class
  'lib/routes.php',                // Rest API routes
  'lib/helpers.php',                // Template Helpers
  'lib/options.php'                // CMB2 Options page
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}

// Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');

register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'Sage Starter Theme' ),
) );

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Site Code',
		'menu_title'	=> 'Global Option',
		'menu_slug' 	=> 'theme-site-code',
		'capability'	=> 'manage_options',
		'redirect'		=> false
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Site Footer',
		'parent_slug'	=> 'theme-site-code'
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Notification Bar Settings',
		'menu_title'	=> 'Notification Bar',
		'parent_slug'	=> 'theme-site-code'
	));

}
function enqueue_masonry() {
    wp_enqueue_script('jquery-masonry');
}
add_action('wp_enqueue_scripts','enqueue_masonry');

// hook for accessable enpoints, note that permalinks should be flushed on any change!
add_action( 'init', 'rewrite_for_fb_twt_auth' );



function rewrite_for_fb_twt_auth() {
  add_rewrite_rule( '^fb-authenticate$', 'index.php?fb-authenticate=1', 'top' );
  add_rewrite_rule( '^fb-authentication-response$', 'index.php?fb-authentication-response=1', 'top' );
  add_rewrite_rule( '^twt-authenticate$', 'index.php?twt-authenticate=1', 'top' );
  add_rewrite_rule( '^twt-authentication-response$', 'index.php?twt-authentication-response=1', 'top' );
  add_rewrite_rule( '^log-me-out$', 'index.php?log-me-out=1', 'top' );
}

// hook gor query vars to be usable globally 
add_action( 'query_vars', 'query_vars_for_fb_twt_auth' );

function query_vars_for_fb_twt_auth($vars) {
  $vars[] = 'fb-authenticate';
  $vars[] = 'fb-authentication-response';
  $vars[] = 'twt-authenticate';
  $vars[] = 'twt-authentication-response';
  $vars[] = 'log-me-out';
  return $vars;
}

// hook for template override, priority should be higher than 109 as sage wil otherwise include header and footer
add_action( 'template_include', 'change_template_for_fb_twt_auth', 110 );

function change_template_for_fb_twt_auth( $template ) {
  if( get_query_var( 'fb-authenticate', false ) !== false ) {
    $newTemplate = locate_template( array( 'template-fb-authenticate.php' ) );
    if( '' != $newTemplate )
      return $newTemplate;
  }
  if( get_query_var( 'fb-authentication-response', false ) !== false ) {
    $newTemplate = locate_template( array( 'template-fb-authentication-response.php' ) );
    if( '' != $newTemplate )
      return $newTemplate;
  }
  if( get_query_var( 'twt-authenticate', false ) !== false ) {
    $newTemplate = locate_template( array( 'template-twt-authenticate.php' ) );
    if( '' != $newTemplate )
      return $newTemplate;
  }
  if( get_query_var( 'twt-authentication-response', false ) !== false ) {
    $newTemplate = locate_template( array( 'template-twt-authentication-response.php' ) );
    if( '' != $newTemplate )
      return $newTemplate;
  }
  if( get_query_var( 'log-me-out', false ) !== false ) {
    $newTemplate = locate_template( array( 'template-log-me-out.php' ) );
    if( '' != $newTemplate )
      return $newTemplate;
  }
  return $template;
}

add_filter( 'rest_endpoints', 'disable_users_rest_endpoints' );

function disable_users_rest_endpoints( $endpoints ) {
  if ( isset( $endpoints['/wp/v2/users'] ) ) {
    unset( $endpoints['/wp/v2/users'] );
  }
  // if ( isset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] ) ) {
  //   unset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] );
  // }
  return $endpoints; 
}

if ( ! function_exists('write_log')) {
   function write_log ( $log )  {
      if ( is_array( $log ) || is_object( $log ) ) {
         error_log( print_r( '['.date("Y-m-d H:i:s").'] // ' . $log . "<br /> \n", true ) );
      } else {
         error_log( $log );
      }
   }
}

add_action( 'admin_menu', 'add_debug_admin_menu' );

function add_debug_admin_menu() {
  add_menu_page( 'Debug Log', 'Debug Log', 'manage_options', 'debug-log', 'debug_log_page', 'dashicons-info', 999 );
}

function debug_log_page() {
  ?>
  <div class="wrap">
    <h2>Debug Log</h2>
    <?php
      if ( file_exists( WP_CONTENT_DIR . '/debug.log' ) ) {
        $fh = fopen(WP_CONTENT_DIR . '/debug.log','r');
        while ($line = fgets($fh)) {
          echo($line) . '<br>';
        }
        fclose($fh);
      } else { ?>
      <p>Nothing in the log!</p>
    <?php } ?>
  </div>
  <?php
}

//Enables to upload SVG
function add_svg_to_upload_mimes( $upload_mimes ) {
	$upload_mimes['svg'] = 'image/svg+xml';
	$upload_mimes['svgz'] = 'image/svg+xml';
	return $upload_mimes;
}
add_filter( 'upload_mimes', 'add_svg_to_upload_mimes', 10, 1 );

// add_action( 'pre_get_posts', 'my_pre_get_posts', 999 );
// function my_pre_get_posts($query) {
//   if ($query->get('post_type') === 'nav_menu_item') {
//     $query->set( 'tax_query', '' );
//     $query->set( 'meta_key', '' );
//     $query->set( 'orderby', '' );
//   }
// }


function categories() {
  $categories = $_REQUEST["Categories"];
$posts = new WP_Query( ['post_type' => 'mgl_product', 'cat' => $categories] );
		
return $posts;
die(); 
}

add_action('wp_ajax_categories', 'categories');
add_action('wp_ajax_nopriv_categories', 'categories');

/**
  * Add REST API support to an already registered post type.
  */
//   add_action( 'init', 'my_custom_post_type_rest_support', 25 );
//   function my_custom_post_type_rest_support() {
//   	global $wp_post_types;
  
//   	//be sure to set this to the name of your post type!
//   	$post_type_name = 'mgl_brand';
//   	if( isset( $wp_post_types[ $post_type_name ] ) ) {
//   		$wp_post_types[$post_type_name]->show_in_rest = true;
//   		$wp_post_types[$post_type_name]->rest_base = $post_type_name;
//   		$wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
//   	}
  
//   }
//    
//    
/**
  * Add REST API support to an already registered taxonomy.
  */
//   add_action( 'init', 'my_custom_taxonomy_rest_support', 25 );
//   function my_custom_taxonomy_rest_support() {
//   	global $wp_taxonomies;
  
//   	//be sure to set this to the name of your taxonomy!
//   	$taxonomy_name = 'mgl_product_category';
  
//   	if ( isset( $wp_taxonomies[ $taxonomy_name ] ) ) {
//   		$wp_taxonomies[ $taxonomy_name ]->show_in_rest = true;
//   		$wp_taxonomies[ $taxonomy_name ]->rest_base = $taxonomy_name;
//   		$wp_taxonomies[ $taxonomy_name ]->rest_controller_class = 'WP_REST_Terms_Controller';
//   	}
  
  
//   }