<?php
/**
 * Template Name: Brand Submit without payment
 */

$user = wp_get_current_user();
$braintree_client_token = Braintree_ClientToken::generate();

?>
<div id="braintree-client-token" data-token="<?= $braintree_client_token ?>"></div>

<div id="template-brand_submit">

    <section id="brand_price" class="pT pB">
        <div class="container">
            <div class="row borderB">
                <!-- brand detail title section-->
                <div class="col-sm-4 text-sm-center white-text">
					<?php the_field( 'bs_page_header' ); ?>
                </div>
                <div class="col-sm-1">
                    &nbsp;
                </div>
                <div class="col-sm-7 white-text priceList">
					<?php the_field( 'bs_benefit_content' ); ?>
                </div>
            </div>
            <div class="row borderB companyLogo">
                <div class="col-sm-10 col-sm-offset-1 text-center white-text">
					<?php the_field( 'bs_supported_company_content' ); ?>
                </div>
            </div>
            <!-- price -->
            <div class="row msB pricing">
                <div class="col-nano-12 text-center">
                    <div class="well">
						<?php the_field( 'bs_price_content' ); ?>
                        <div class="row">
                          <div class="col-sm-5 text-center pt5">
                            <?php the_field( 'bs_price_left' ); ?>
                          </div>
                          <div class="col-sm-2 text-center pt5">
                            <p class="plus">+</p>
                          </div>
                          <div class="col-sm-5 text-center pt5">
                            <?php the_field( 'bs_price_right' ); ?>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <form id="checkout-form" method="post" action="/mgl/v1/subscription/new">
        <input type="hidden" name="payment-method-nonce" id="nonce">

        <section class="checkout-section">
            <div class="container">
                <header class="checkout-section__header">
                    <h2><strong>Step 1:</strong> Tell us about your brand</h2>
                </header>
                <div class="row checkout-user">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="first-name">First Name</label>
                            <input type="text" class="form-control" id="first-name" name="firstName" placeholder="John">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="last-name">Last Name</label>
                            <input type="text" class="form-control" id="last-name" name="lastName" placeholder="Doe">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email-address">Email Address</label>
                            <input type="text" class="form-control" id="email-address" name="email"
                                   placeholder="john.doe@me.com">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="phone-number">Phone Number</label>
                            <input type="text" class="form-control" id="phone-number" name="phone"
                                   placeholder="999-999-9999">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="brand-name">Brand Name</label>
                            <input type="text" class="form-control" id="brand-name" name="brandName" placeholder="Toms">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="job-title">Job Title</label>
                            <input type="text" class="form-control" id="job-title" name="job"
                                   placeholder="VP, Marketing">
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="website-url">Website URL</label>
                            <input type="text" class="form-control" id="website-url" name="website"
                                   placeholder="www.toms.com">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="social-textarea">Social or Ecological Impact (140 characters max)</label>
                            <textarea name="impact" class="form-control" id="social-textarea"
                                      placeholder="For every item that you purchase, we donate one to a person in need."></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="checkout-section">
            <div class="container">
                <header class="checkout-section__header">
                    <h2><strong>Step 2:</strong> Categories (select all that apply)</h2>
                </header>
                <div class="checkout-categories">
                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-14" value="14">
                        <label class="category" for="category-14">Accessories</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-11" value="11">
                        <label class="category" for="category-11">Beauty</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-8" value="8">
                        <label class="category" for="category-8">Clothing</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-60" value="60">
                        <label class="category" for="category-60">Food</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-17" value="17">
                        <label class="category" for="category-17">Gifts</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-41" value="41">
                        <label class="category" for="category-41">Health</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-9" value="9">
                        <label class="category" for="category-9">Home</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-18" value="18">
                        <label class="category" for="category-18">Jewelry</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-10" value="10">
                        <label class="category" for="category-10">Kids</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-27" value="27">
                        <label class="category" for="category-27">Pets</label>
                    </div>

                    <div class="checkout-categories__item">
                        <input type="checkbox" name="mgl_product_category[]" id="category-24" value="24">
                        <label class="category" for="category-24">Shoes</label>
                    </div>
                </div>
            </div>
        </section>
      
        <section class="checkout-section">
            <div class="container">
                <header class="checkout-section__header">
                    <h2><strong>Step 3:</strong> Which e-commerce platform do you use?</h2>
                </header>  
                <div class="row checkout-platform">
                  <div class="col-md-8">
                  <div class="form-group">
                    <label for="platform-select">Please Select One</label>
                    <select class="form-control" name="platform" id="platform-select">
                      <option value="" selected="" data-reactid="">--</option>
                      <option value="shopify" data-reactid="">Shopify</option>
                      <option value="squarespace" data-reactid="">Squarespace</option>
                      <option value="woocommerce" data-reactid="">WooCommerce</option>
                      <option value="bigcommerce" data-reactid="">BigCommerce</option>
                      <option value="wix" data-reactid="">Wix</option>
                      <option value="magento" data-reactid="">Magento</option>
                      <option value="volusion" data-reactid="">Volusion</option>
                      <option value="bigcartel" data-reactid="">BigCartel</option>
                      <option value="3d-cart" data-reactid="">3D Cart</option>
                      <option value="ecwid" data-reactid="">ECWid</option>
                      <option value="not-know" data-reactid="">I don’t know</option>
                      <option value="none" data-reactid="">None</option>
                      <option value="other" data-reactid="">Other</option>
                    </select>
                  </div>
                </div>
                  <div class="col-md-8"  id="other-reason">
                  <div class="form-group">
                    <label for="other-reason">Tell us more…</label>
                    <textarea name="impact" class="form-control" id="social-textarea" placeholder=""></textarea>
                  </div>
                </div>
              </div>
            </div>
        </section>

        <section class="checkout-section checkout-section--payment">
            <div class="container">
                <div class="checkout-submit">
                    <button type="submit">Submit Your Brand <span class="fa fa-chevron-right"></span></button>
                </div>
            </div>
        </section>
    </form>

</div>

<script type="text/javascript">
jQuery(document).ready(function() {

  jQuery("select#platform-select").change(function (){
		jQuery("#other-reason").hide();
		if(jQuery(this).val() == 'other') {
			jQuery("#other-reason").slideDown("slow");
		}
		else {
			jQuery("#other-reason").slideUp();
		}
	});
  
 });
</script>