<?php
/**
 * Template Name: Product Page - Marketplace
 */

use Miigle\Models\Product;
use Miigle\Models\Brand;

$gallery = Product\get_image_gallery($post->ID);
$categories = Product\get_categories(get_the_ID());

?>

<div id="template-product_page" class="v2">

    <section id="product_page">
        <div class="container">
            <div class="row">
                <div class="col-md-5 prod-gallery hidden-xs hidden-sm">

                    <div class="row"><!-- Large View -->
                        <div class="col-xs-12">
                            <div class="full">
                                <img src="<?php echo get_the_post_thumbnail_url() ?>" class="img-responsive"/>
                            </div>
                        </div>
                    </div>
                    <div class="row"><!-- Thumbnail View -->
                        <?php foreach ($gallery as $k => $v): ?>
                            <div class="col-nano-3 previews">
                                <a href="#" <?php if ($k == 0): ?>class="selected"<?php endif; ?>
                                   data-full="<?= $v['image'] ?>">
                                    <img src="<?= $v['image'] ?>" class="img-responsive"/>
                                </a>
                            </div>
                        <?php endforeach; ?>
                        <!-- MAX IMG 4 -->
                    </div>

                </div>
                <div class="col-md-7 prod-data">
                    <div class="prod--info text-upper">
                        <h3 class="nekaj">
                            <?php echo Product\get_brand_name(get_the_ID()) ?>
                        </h3>
                        <div class="mFlex">
                            <h1>
                                <?php the_title() ?>
                            </h1>
                            <p class="prod-price">
                                <?php echo Product\get_price(get_the_ID()) ?>
                            </p>
                        </div>
                    </div>
                    <div class="prod--category pb3">
                        <?php foreach (Product\get_categories(get_the_ID()) as $category): ?>
                            <?php if (!$category->parent): ?>
                                <a href="#"
                                   class="btn btn-category btn-<?= $category->name ?> text-lower">  <?= $category->name ?> </a>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <!-- .prod-gallery Show ONLY MOBILE -->
                    <div class="prod-gallery visible-xs visible-sm pb3">
                        <div class="row"><!-- Large View -->
                            <div class="col-xs-12">
                                <div class="full">
                                    <img src="<?php echo get_the_post_thumbnail_url() ?>" class="img-responsive"/>
                                </div>
                            </div>
                        </div>
                        <div class="row"><!-- Thumbnail View -->
                            <div class="col-nano-3 previews">
                                <?php foreach ($gallery as $k => $v): ?>
                                    <div class="col-nano-3 previews">
                                        <a href="#" <?php if ($k == 0): ?>class="selected"<?php endif; ?>
                                           data-full="<?= $v['image'] ?>">
                                            <img src="<?= $v['image'] ?>" class="img-responsive"/>
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                                <!-- MAX IMG 4 -->
                            </div>

                        </div>
                    </div>
                    <!-- END.prod-gallery  -->
                    <div class="prod--description pb3">

                        <?php the_content() ?>

                        <div class="hidden-content">
                            <p>Discover great products from people just like you who care about our planet Discover
                                great products from people. Discover great products from people just like you who care
                                about our planet Discover great products from people. Discover great products from
                                people just like you who care about.</p>
                        </div>

                        <!--<a class="readMore btn-read-more text-upper" href="#read-more">Read More</a>-->

                    </div>
                    <div class="prod--action">
                        <a href="<?php echo Product\get_url(get_the_ID()) ?>" class="btn btn-action btn-buy text-upper">Buy</a>
                        <!--<a href="#share" class="btn btn-action btn-share text-upper">Share</a>-->
                        <!--<a href="#like" class="btn btn-action btn-like text-upper">Like</a>-->

                        <?= do_shortcode('[TheChamp-Sharing]'); ?>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="section--search-bar" class="pt5">
        <div class="container">
            <div class="search-wrapper">
                <form id="product-search" method="GET" action="/search">
                    <div class="row">
                        <div class="col-nano-9">
                            <div class="input-group">
                                <span class="input-group-addon"><img src="/wp-content/uploads/2018/07/icon-search.svg"></span>
                                <input type="search" class="form-control" name="search" placeholder="Search ethical products and brands…">
                                <!--<input type="search" class="form-control" name="s" placeholder="Search ethical products and brands…" onfocus="if (this.value == 'Search') { this.value = ''; }" onblur="if (this.value == '') this.value='Search';">-->
                            </div>
                        </div>
                        <div class="col-nano-3">
                            <button type="submit" class="btn btn-default btn-submit">SEARCH</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <section id="section--similar-products" class="section--pad">
        <div class="container">
            <!-- TITLE ROW -->
            <div class="row">
                <div class="col-nano-12">
                    <div class="title--pad">
                        <h2 class="background"><span>Similar Products</span></h2>
                    </div>
                </div>
            </div>

            <div class="row masonry-container">

                <div class="col-nano-12 col-micro-6 col-xs-6 col-sm-6 col-md-4 col-lg-3 masonry-sizer"></div>
                <!-- left empty for column sizing -->
                <?php
                wp_reset_postdata();
                $filters = '';
                foreach ($categories as $category) {

                    $filters .= $category->slug . ',';
                }
                $filters = substr($filters, 0, -1);

                $the_query = new WP_Query(['post_type' => 'mgl_product', 'mgl_product_category' => $filters]);

                if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
                    ?>

                    <div class="col-lg-3 col-md-4 col-nano-6 masonry-item">
                        <div class="thumbnail">
                            <img src="<?php echo get_the_post_thumbnail_url() ?>" class="">
                            <div class="caption">
                                <p class="brand">
                                    <?php echo Product\get_brand_title(get_the_ID()) ?>

                                </p>

                                <h3><?php the_title() ?></h3>
                                <p class="price"><?= Product\get_price(get_the_ID()) ?></p>
                            </div>
                            <a href="<?php echo get_the_permalink() ?>">
                                <div class="mask"></div>
                            </a>
                        </div>
                    </div>
                <?php endwhile; ?>
                <?php endif;
                wp_reset_postdata(); ?>

            </div>

            <div class="row view-more" id="viewProducts">
                <div class="col-nano-12 text-center">
                    <a class="btn-view-more text-upper" href="<?php echo site_url('marketplace') ?>">View
                        More</a>
                </div>
            </div>

        </div>
    </section>

</div>






