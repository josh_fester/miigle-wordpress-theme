<?php
/**
 * Template Name: View More - Marketplace
 */

use Miigle\Models\Brand;
use Miigle\Models\Product;
use Miigle\Helpers;
//$user = wp_get_current_user();

if(isset($_GET['category']))
    $category =$_GET['category'];
else
	$category = '';

$category = str_replace('\\"', '', $category);

?>

<div id="template-view_more" class="v2 category-page">

    <section id="section--search-bar" class="pt5 pb5">
        <?php get_template_part('templates/include', 'search'); ?>
    </section>

    <section id="section--filter" class="">
      <div class="container">
        
        <!-- TITLE ROW -->
        <div class="row">
          <div class="col-nano-12">
            <div class="text-upper title--pad">
              <h1 id="result-amount">BROWSE ALL PRODUCTS</h1>
            </div>
          </div>
        </div>
        
        <!-- Category List Carousel for Desktop -->
        <div class="row pb5 hidden-xs category-list">
          <div class="col-nano-12">
            <div id="myCarousel" class="carousel slide">
              <div class="carousel-inner">

                <div class="item active">
                  <div class="row">
                    <div class="col-nano-2">
                      <div class="card" style="background-image:url('https://dev.miigle.com/wp-content/themes/sage/dist/images/brand-accessories.png');">
                        <div class="mask accessories"></div>
                        <p>Accessories</p>
                        <a href="/marketplace?category=accessories">
                          <div class="mask"></div>
                        </a>
                      </div>
                    </div>
                    <div class="col-nano-2">
                      <div class="card" style="background-image:url('https://dev.miigle.com/wp-content/themes/sage/dist/images/brand-beauty.png');">
                        <div class="mask beauty"></div>
                        <p>Beauty</p>
                        <a href="/marketplace?category=beauty">
                          <div class="mask"></div>
                        </a>
                      </div>
                    </div>
                    <div class="col-nano-2">
                      <div class="card" style="background-image:url('https://dev.miigle.com/wp-content/themes/sage/dist/images/brand-clothing.png');">
                        <div class="mask clothing"></div>
                        <p>Clothing</p>
                        <a href="/marketplace?category=clothing">
                          <div class="mask"></div>
                        </a>
                      </div>
                    </div>
                    <div class="col-nano-2">
                      <div class="card" style="background-image:url('https://dev.miigle.com/wp-content/themes/sage/dist/images/brand-electronics.png');">
                        <div class="mask electronics"></div>
                        <p>Electronics</p>
                        <a href="/marketplace?category=electronics">
                            <div class="mask"></div>
                      </a>
                      </div>
                    </div>
                    <div class="col-nano-2">
                      <div class="card" style="background-image:url('https://dev.miigle.com/wp-content/themes/sage/dist/images/brand-foods.png');">
                        <div class="mask foods"></div>
                        <p>Foods</p>
                        <a href="/marketplace?category=foods">
                          <div class="mask"></div></a>
                      </div>
                    </div>
                    <div class="col-nano-2">
                      <div class="card" style="background-image:url('https://dev.miigle.com/wp-content/themes/sage/dist/images/brand-gifts.png');">
                        <div class="mask gifts"></div>
                        <p>Gifts</p>
                        <a href="/marketplace?category=gifts">
                          <div class="mask"></div></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="row">
                    <div class="col-nano-2">
                      <div class="card" style="background-image:url('https://dev.miigle.com/wp-content/themes/sage/dist/images/brand-health.png');">
                        <div class="mask health"></div>
                        <p>Health</p>
                        <a href="/marketplace?category=health">
                          <div class="mask"></div>
                        </a>
                      </div>
                    </div>
                    <div class="col-nano-2">
                      <div class="card" style="background-image:url('https://dev.miigle.com/wp-content/themes/sage/dist/images/brand-home.png');">
                        <div class="mask home"></div>
                        <p>Home</p>
                        <a href="/marketplace?category=home">
                          <div class="mask"></div></a>
                      </div>
                    </div>
                    <div class="col-nano-2">
                      <div class="card" style="background-image:url('https://dev.miigle.com/wp-content/themes/sage/dist/images/brand-kids.png');">
                        <div class="mask kids"></div>
                        <p>Kids</p>
                        <a href="/marketplace?category=kids">
                          <div class="mask"></div>
                        </a>
                      </div>
                    </div>
                    <div class="col-nano-2">
                      <div class="card" style="background-image:url('https://dev.miigle.com/wp-content/themes/sage/dist/images/brand-jewelry.png');">
                        <div class="mask jewelry"></div>
                        <p>Jewelry</p>
                        <a href="/marketplace?category=jewelry">
                          <div class="mask"></div>
                        </a>
                      </div>
                    </div>
                    <div class="col-nano-2">
                      <div class="card" style="background-image:url('https://dev.miigle.com/wp-content/themes/sage/dist/images/brand-pets.png');">
                        <div class="mask pets"></div>
                        <p>Pets</p>
                        <a href="/marketplace?category=pets">
                          <div class="mask"></div>
                        </a>
                      </div>
                    </div>
                    <div class="col-nano-2">
                      <div class="card" style="background-image:url('https://dev.miigle.com/wp-content/themes/sage/dist/images/brand-shoes.png');">
                        <div class="mask shoes"></div>
                        <p>Shoes</p>
                        <a href="/marketplace?category=shoes">
                          <div class="mask"></div>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>

              </div>

              <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
              <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
            </div>
          </div>
        </div>
        
        <!-- Category List for Mobile visible-xs-->
        <div class="row category-list pb2 visible-xs">
          <div class="col-nano-12 flex flex--justify-right">
            <ul class="nav nav-pills list-group">
              <li class="list-group-item list-prod">
                <a class="dropdown-toggle list-title" data-toggle="dropdown" href="#product-category" role="button" aria-haspopup="true" aria-expanded="false">Product Category <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li class="list-item"><a href="/marketplace?category=all">All</a></li>
                    <li class="list-item"><a href="/marketplace?category=accessories">Accessories</a></li>
                    <li class="list-item"><a href="/marketplace?category=beauty">Beauty</a></li>
                    <li class="list-item"><a href="/marketplace?category=clothing">Clothing</a></li>
                    <li class="list-item"><a href="/marketplace?category=electronics">Electronics</a></li>
                    <li class="list-item"><a href="/marketplace?category=foods">Foods</a></li>
                    <li class="list-item"><a href="/marketplace?category=gifts">Gifts</a></li>
                    <li class="list-item"><a href="/marketplace?category=health">Health</a></li>
                    <li class="list-item"><a href="/marketplace?category=home">Home</a></li>
                    <li class="list-item"><a href="/marketplace?category=kids">Kids</a></li>
                    <li class="list-item"><a href="/marketplace?category=jewelry">Jewelry</a></li>
                    <li class="list-item"><a href="/marketplace?category=pets">Pets</a></li>
                    <li class="list-item"><a href="/marketplace?category=shoes">Shoes</a></li>
                  </ul>
              </li>
            </ul>
          </div>
        </div>
        
        <!-- Filter -->
        <div class="row">
          <div class="col-nano-12 flex flex--justify-right flex--align-vertical" id="filter-product">
            <form class="form-inline" id="product-filter">
              <p class="text-upper fitler-txt">Sort by</p>
              <div class="form-group filter-type custom-select" id="sort-by">
                  <select class="form-control" name="sort" id="sort-select">
                      <option value="" data-reactid="">Most Popular</option>
                      <option value="most-popular" data-reactid="">Most Popular</option>
                      <option value="newest" data-reactid="">Newest</option>
                  </select>
              </div>
              <p class="text-upper fitler-txt">Persona</p>
              <div class="form-group filter-type custom-select" id="persona">
                  <select class="form-control" name="persona" id="persona-select">
                      <option value="" data-reactid="" class="persona">All</option>
                      <option value="all" data-reactid="" class="persona">All</option>
                      <option value="women" data-reactid="" class="persona">Women</option>
                      <option value="men" data-reactid="" class="persona">Men</option>
                      <option value="kids" data-reactid="" class="persona">Kids</option>
                  </select>
              </div>
          </form>
          </div>
        </div>
     
      </div>
    </section>     
    
  <section id="section--results" class="section--pad">
    <div class="container">
      <div class="row sr-only">
        <div id="product-side-filter">
                <ul class="list-group">
                    <li class="list-group-item list-title">Sort By</li>
                    <li class="list-group-item active"><a class="sort" href="most-popular">Most Popular</a></li>
                    <li class="list-group-item"><a class="sort" href="newest">Newest</a></li>
                </ul>

                <ul class="list-group">
                    <li class="list-group-item list-title">Persona</li>
                    <li class="list-group-item active"><a class="persona" href="">All</a></li>
                    <li class="list-group-item"><a class="persona" href="women">Women</a></li>
                    <li class="list-group-item"><a class="persona" href="men">Men</a></li>
                    <li class="list-group-item"><a class="persona" href="kids">Kids</a></li>
                </ul>
                <ul class="list-group">
                    <li class="list-group-item list-title">Product Category</li>
                    <li class="list-group-item active"><a class="category" href="">All</a></li>
                    <li class="list-group-item"><a class="category" href="accessories">Accessories</a></li>
                    <li class="list-group-item"><a class="category" href="beauty">Beauty</a></li>
                    <li class="list-group-item"><a class="category" href="clothing">Clothing</a></li>
                    <li class="list-group-item"><a class="category" href="electronics">Electronics</a></li>
                    <li class="list-group-item"><a class="category" href="foods">Foods</a></li>
                    <li class="list-group-item"><a class="category" href="gifts">Gifts</a></li>
                    <li class="list-group-item"><a class="category" href="health">Health</a></li>
                    <li class="list-group-item"><a class="category" href="home">Home</a></li>
                    <li class="list-group-item"><a class="category" href="kids">Kids</a></li>
                    <li class="list-group-item"><a class="category" href="jewelry">Jewelry</a></li>
                    <li class="list-group-item"><a class="category" href="pets">Pets</a></li>
                    <li class="list-group-item"><a class="category" href="shoes">Shoes</a></li>
                </ul>
            </div>
      </div>
      <div class="row">
        <!-- RESULT COL -->
        <div class="col-nano-12" id="result-product">
          <div class="row masonry-container" id="posts-container">
              <div class="col-nano-12 col-micro-6 col-xs-6 col-sm-6 col-md-4 col-lg-3 masonry-sizer"></div>
              <!-- left empty for column sizing -->

            <?php wp_reset_postdata();

            if(isset($category))

                $args = array('post_type' => 'mgl_product', 'mgl_product_category' => $category, 'orderby' => 'rand', 'posts_per_page' => 120);
            else
                $args = array('post_type' => 'mgl_product', 'orderby' => 'rand', 'posts_per_page' => 120);
            $the_query = new WP_Query($args);
            if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post(); ?>
                  <div class="col-lg-3 col-md-4 col-nano-6 masonry-item">
                      <div class="thumbnail">
                          <img src="<?php echo Product\get_thumbnail(get_the_ID()) ?>" class="">
                          <div class="caption">
                              <p class="brand">
                                  <?php echo Product\get_brand_title(get_the_ID()) ?>

                              </p>

                              <h3><?php the_title() ?></h3>
                              <p class="price"><?= Product\get_price(get_the_ID()) ?></p>
                          </div>
                          <a href="<?php echo get_the_permalink() ?>">
                              <div class="mask"></div>
                          </a>
                      </div>
                  </div>
              <?php endwhile; ?>
              <?php endif;
            wp_reset_postdata(); ?>

          </div>
          <div class="row view-more" id="viewProducts">
              <div class="col-nano-12 text-center">
                  <a id="view" class="btn-view-more text-upper counter-button" href="#view">View More</a>
              </div>
          </div>
        </div>
      </div>
    </div>
  </section>

</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        var counter = 0;
        var persona = '';
        var cat = '<?php if (isset($category)) {echo $category;} else echo '' ?>';
        var sort = '';
        var sortScope= '';
        var pageCounter = 0;
        var amount = 0;
        var title = jQuery('#result-amount');
      
        getMorePosts();

        // sort by buttons
        jQuery("#sort-by .select-items > div").on('click', function (e) {
            e.preventDefault();
            sort = jQuery(".sort-select").attr("value");
            pageCounter = 0;
            amount = 0;
            getMorePosts();
        });
      
        // persona buttons
        jQuery("#persona .select-items > div").on('click', function (e) {
            e.preventDefault();
            persona = jQuery(".persona-select").attr("value");
            pageCounter = 0;
            amount = 0;
            getMorePosts();
        });
      
        // persona buttons
        /*jQuery(".persona").on('click', function (e) {
            e.preventDefault();
            persona = jQuery(this).attr('href');
            jQuery('.persona').parent().siblings().removeClass('active');
            jQuery(this).parent().addClass('active');
            pageCounter = 0;
            amount = 0;
            getMorePosts();
        });*/

        // category buttons
        /*jQuery(".category").on('click', function (e) {
            e.preventDefault();
            cat = jQuery(this).attr('href');
            jQuery('.category').parent().siblings().removeClass('active');
            jQuery(this).parent().addClass('active');
            pageCounter = 0;
            amount = 0;
            getMorePosts();
        });*/

        // sort by buttons
        /*jQuery(".sort").on('click', function (e) {
            e.preventDefault();
            sort = jQuery(this).attr('href');
            jQuery('.sort').parent().siblings().removeClass('active');
            jQuery(this).parent().addClass('active');
            pageCounter = 0;
            amount = 0;
            getMorePosts();
        });*/

        // view more button
        jQuery(".counter-button").on('click', function (e) {
            pageCounter++;
            getMorePosts();
        });

        /*jQuery("#category-select").on('change', function (e) {
            cat = jQuery("#category-select option:selected").val();
            pageCounter = 0;
            amount = 0;
            getMorePosts();
        });*/

        /*jQuery("#sort-by").on('change', function (e) {
            sort = jQuery("#sort-by option:selected").val();
            pageCounter = 0;
            amount = 0;
            getMorePosts();
        });*/

        function getMorePosts(){
            var scope = '';

            // If-else statements for scope modifications
            if (persona !== '' && cat !== '' && cat !== 'all')
                scope = persona + '-' + cat;
            else if(persona === '')
                scope = cat;
            else if(persona === '' && cat === '')
                scope = 'all';
            else
                scope = persona;

            // Sets the sort scope
            sortScope = '&sort="' + sort + '"';

            // If the scope is not set sets it to all
            if (scope === '')
                scope = 'all';

            /**
             * Ajax call
             */

            jQuery.when(
                jQuery.ajax({
                    url: '/getposts.php?scope="' + scope + '"' + sortScope + '&page="' + pageCounter + '"',
                    success: function(response){
                        var data = jQuery.parseJSON(response);
                        // container selector
                        var container = jQuery("#posts-container");

                        //if the data is loaded for the first time clears the container and sets height to auto
                        if (pageCounter === 0)
                        {
                            container.html(' <div class="col-nano-12 col-micro-6 col-xs-6 col-sm-6 col-md-4 col-lg-3 masonry-sizer"></div>');
                            container.css({'height' : 'auto'});
                        }

                        counter = 0;

                        // Displays data with an .each loop
                        jQuery.when(  jQuery.each(data, function (key, product)
                        {
                           if (key !== 'counter')
                           {
                               counter++;
							   var tmpUrl = new URL(product.guid+'');
							   //console.log(tmpUrl);
                               container.append('<div class="col-lg-3 col-md-4 col-nano-6 masonry-item">'+
                                   '<div class="thumbnail">'+
                                   '<img src="' + product.thumbnail + '" class="">'+
                                   '<div class="caption">'+
                                   '<p class="brand">'+
                                   product.brand +
                                   '</p>'+

                                   '<h3>' +
                                   product.post_title +
                                   '</h3>' +
                                   '<p class="price">' +
                                   product.price +
                                   '</p>'+
                                   '</div>' +
                                   '<a href="' + tmpUrl.pathname + '"><div class="mask"></div></a>' +
                                   '</div>' +
                                   '</div>');
                           }
                        })).then(function( ) {
                            amount += counter;
                            jQuery(title).text('');
                            jQuery(title).append('BROWSING <span class="clr">' + data.counter + '</span> ' + cat + ' PRODUCTS');
                        });


                    }
                })
            ).then(masonry2);



        }

        function masonry2(){
            var masonry = jQuery('#posts-container');

            jQuery(masonry).imagesLoaded( function() {
                jQuery(masonry).masonry({
                    columnWidth: '.masonry-sizer',
                    itemSelector: '.masonry-item',
                    percentPosition: true
                });
            });
            jQuery(masonry).masonry('reload');
            jQuery(masonry).masonry('layout');

        }
    });

</script>
